package com.kreativeco.legionkrea;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RecoverPass extends Activity implements WebBridge.WebBridgeListener{
    private EditText email;
    private TextView resp;
    public float d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        d = getResources().getDisplayMetrics().density;
        setContentView(R.layout.recover_pass);
        resp = (TextView)findViewById(R.id.txt_response);
        email = (EditText) findViewById(R.id.edt_email);
        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                LinearLayout r = (LinearLayout)view.getParent();
                LayoutParams params = (LayoutParams)r.getLayoutParams();
                if (b) {
                    r.setBackgroundResource(R.drawable.registro_campo_sombra);
                    params.setMargins((int)(20*d), (int)(-12*d), (int)(20*d), (int)(-12*d));
                    r.setLayoutParams(params);
                    r.invalidate();
                } else {
                    r.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    params.setMargins((int)(50*d), 0, (int)(50*d), 0);
                    r.setLayoutParams(params);
                    r.invalidate();
                }
            }
        });
    }

    public void send (View v){
        HashMap<String, Object> params = new HashMap<>();
        params.put("email", email.getText().toString());
        WebBridge.send("recover=1&key=admin", params, "Enviando", this, this);
    }

    public void close(View view){
        finish();
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")){
                resp.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {

    }
}
