package com.kreativeco.legionkrea;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Home extends Activity implements WebBridge.WebBridgeListener{

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    private FrameLayout noto;
    private RelativeLayout home;
    private TextView title, body, number;
    private int not;
    private Boolean isNot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        noto = (FrameLayout) findViewById(R.id.lay_not);
        home = (RelativeLayout) findViewById(R.id.home);
        title = (TextView) findViewById(R.id.txt_title);
        body = (TextView) findViewById(R.id.txt_body);
        number = (TextView) findViewById(R.id.txt_number);
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("Home_Activity", bundle);
        isNot = false;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        WebBridge.send("notificacionesuser=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Home",this.getClass().getSimpleName());
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }

    public void loadEmpatizar(View view){
        Intent loginIntent = new Intent(this, Personaje.class);
        startActivity(loginIntent);
    }

    public void loadDefinir(View view){
        Intent loginIntent = new Intent(this, MatrizDefinir.class);
        startActivity(loginIntent);
    }

    public void loadIdear(View view){
        Intent loginIntent = new Intent(this, Idear.class);
        startActivity(loginIntent);
    }

    public void loadPrototipar(View view){
        Intent loginIntent = new Intent(this, Prototipar.class);
        startActivity(loginIntent);
    }

    public void loadTestear(View view){
        Intent loginIntent = new Intent(this, Feedback.class);
        startActivity(loginIntent);
    }

    public void loadNProyect(View view){
        Intent loginIntent = new Intent(this, NewProyect.class);
        startActivity(loginIntent);
    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }

    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }

    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }

    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadNotifs(View view){
        Intent loginIntent = new Intent(this, Notificaciones.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    public void dissmissNot(View view){
        noto.setVisibility(View.GONE);
        home.setVisibility(View.VISIBLE);
    }

    public void viewNot(View view){
        isNot=true;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id_notificacion",not);
        WebBridge.send("notificacionvista=1&key=admin", params, "Enviando", this, this);
    }

    public void loadNot(){
        isNot = false;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        WebBridge.send("notificacionesuser=1&key=admin", params, "Enviando", this, this);
    }

    public void closeSession(){
        if (User.logged(this)) {User.clear(this);}
            //LoginManager.getInstance().logOut();
            Intent intent = new Intent(this, LogIn.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")){
                if (isNot){
                    new AlertDialog.Builder(this)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle("Aviso")
                            .setMessage("Notificación Vista")
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    loadNot();
                                }

                            })
                            .show();
                }else {
                    JSONArray nots = json.getJSONArray("data");
                    if (nots.length() > 0) {
                        noto.setVisibility(View.VISIBLE);
                        home.setVisibility(View.GONE);
                        number.setText(nots.length() + "");
                        body.setText(nots.getJSONObject(0).getString("notificacion"));
                        not = nots.getJSONObject(0).getInt("id_notificacion_user");
                    }else{
                        noto.setVisibility(View.GONE);
                        home.setVisibility(View.VISIBLE);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                closeSession();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
