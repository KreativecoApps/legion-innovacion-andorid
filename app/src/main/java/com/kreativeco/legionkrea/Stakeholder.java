package com.kreativeco.legionkrea;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Stakeholder extends Activity implements WebBridge.WebBridgeListener{

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    private int id;
    private String name;
    private TextView title;
    private ImageView st1, st2, st3, st4;
    private JSONObject dato1=null, dato2=null, dato3=null, dato4=null;
    private String ob1, ob2, ob3, ob4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stakeholder);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        id = getIntent().getIntExtra("id",0);
        name = getIntent().getStringExtra("namae");
        title = (TextView) findViewById(R.id.txt_title);
        title.setText(name);
        st1 = (ImageView) findViewById(R.id.st1);
        st2 = (ImageView) findViewById(R.id.st2);
        st3 = (ImageView) findViewById(R.id.st3);
        st4 = (ImageView) findViewById(R.id.st4);
        st1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadNStake(1);
            }
        });
        st2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadNStake(2);
            }
        });
        st3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadNStake(3);
            }
        });
        st4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadNStake(4);
            }
        });
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("Stackholder_Activity", bundle);
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id_proyecto",id);
        WebBridge.send("listadostakholderuser=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Stackholders",this.getClass().getSimpleName());
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }

    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    public void loadNProyect(View view){
        Intent loginIntent = new Intent(this, NewProyect.class);
        startActivity(loginIntent);
    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }

    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }

    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }

    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    public void loadMatriz(View view){
        Intent loginIntent = new Intent(this, MatrizDefinir.class);
        startActivity(loginIntent);
    }

    public void loadNStake (int level){
        Boolean x = false;
        JSONObject z = new JSONObject();
        switch (level){
            case 1:
                if (dato1!=null){
                    x=true;
                    z=dato1;
                }
                break;
            case 2:
                if (dato2!=null){
                    x=true;
                    z=dato2;
                }
                break;
            case 3:
                if (dato3!=null){
                    x=true;
                    z=dato3;
                }
                break;
            case 4:
                if (dato4!=null){
                    x=true;
                    z=dato4;
                }
                break;

        }
        Intent loginIntent = new Intent(this, NuevoStackholder.class);
            try {
                if (x){
                    loginIntent.putExtra("id", id);
                    loginIntent.putExtra("st", z.getInt("id"));
                    loginIntent.putExtra("type", "act");
                    loginIntent.putExtra("stake", z.toString());
                    loginIntent.putExtra("level", level);
                }else {
                    loginIntent.putExtra("id", id);
                    loginIntent.putExtra("type", "new");
                    loginIntent.putExtra("level", level);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        startActivity(loginIntent);
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")){
                JSONArray data = json.getJSONArray("data");
                if (data.length()>0){
                    for (int i = 0; i<data.length();i++){
                        int niv = data.getJSONObject(i).getInt("nivel");
                        switch (niv){
                            case 1:
                                dato1 = data.getJSONObject(i);
                                break;
                            case 2:
                                dato2 = data.getJSONObject(i);
                                break;
                            case 3:
                                dato3 = data.getJSONObject(i);
                                break;
                            case 4:
                                dato4 = data.getJSONObject(i);
                                break;
                        }
                    }
                    if (dato1!=null){
                        int ico = dato1.getInt("icono");
                        int col = dato1.getInt("color");
                        switch (ico) {
                            case 1:
                                Glide.with(this).load(R.drawable.adjunto_ic_imagen_on).into(st1);
                                break;
                            case 2:
                                Glide.with(this).load(R.drawable.adjunto_ic_foto_on).into(st1);
                                break;
                            case 3:
                                Glide.with(this).load(R.drawable.adjunto_ic_video_on).into(st1);
                                break;
                            case 4:
                                Glide.with(this).load(R.drawable.adjunto_ic_audio_on).into(st1);
                                break;
                            case 5:
                                Glide.with(this).load(R.drawable.adjunto_ic_archivo_on).into(st1);
                                break;
                        }
                        switch (col) {
                            case 1:
                                st1.setBackgroundResource(R.drawable.ic_color_rojo);
                                break;
                            case 2:
                                st1.setBackgroundResource(R.drawable.ic_color_verde);
                                break;
                            case 3:
                                st1.setBackgroundResource(R.drawable.ic_color_violeta);
                                break;
                            case 4:
                                st1.setBackgroundResource(R.drawable.ic_color_amarillo);
                                break;
                        }
                    }
                    if (dato2!=null){
                        int ico2 = dato2.getInt("icono");
                        int col2 = dato2.getInt("color");
                        switch (ico2) {
                            case 1:
                                Glide.with(this).load(R.drawable.adjunto_ic_imagen_on).into(st2);
                                break;
                            case 2:
                                Glide.with(this).load(R.drawable.adjunto_ic_foto_on).into(st2);
                                break;
                            case 3:
                                Glide.with(this).load(R.drawable.adjunto_ic_video_on).into(st2);
                                break;
                            case 4:
                                Glide.with(this).load(R.drawable.adjunto_ic_audio_on).into(st2);
                                break;
                            case 5:
                                Glide.with(this).load(R.drawable.adjunto_ic_archivo_on).into(st2);
                                break;
                        }
                        switch (col2) {
                            case 1:
                                st2.setBackgroundResource(R.drawable.ic_color_rojo);
                                break;
                            case 2:
                                st2.setBackgroundResource(R.drawable.ic_color_verde);
                                break;
                            case 3:
                                st2.setBackgroundResource(R.drawable.ic_color_violeta);
                                break;
                            case 4:
                                st2.setBackgroundResource(R.drawable.ic_color_amarillo);
                                break;
                        }
                    }
                    if (dato3!=null){
                        int ico3 = dato3.getInt("icono");
                        int col3 = dato3.getInt("color");
                        switch (ico3) {
                            case 1:
                                Glide.with(this).load(R.drawable.adjunto_ic_imagen_on).into(st3);
                                break;
                            case 2:
                                Glide.with(this).load(R.drawable.adjunto_ic_foto_on).into(st3);
                                break;
                            case 3:
                                Glide.with(this).load(R.drawable.adjunto_ic_video_on).into(st3);
                                break;
                            case 4:
                                Glide.with(this).load(R.drawable.adjunto_ic_audio_on).into(st3);
                                break;
                            case 5:
                                Glide.with(this).load(R.drawable.adjunto_ic_archivo_on).into(st3);
                                break;
                        }
                        switch (col3) {
                            case 1:
                                st3.setBackgroundResource(R.drawable.ic_color_rojo);
                                break;
                            case 2:
                                st3.setBackgroundResource(R.drawable.ic_color_verde);
                                break;
                            case 3:
                                st3.setBackgroundResource(R.drawable.ic_color_violeta);
                                break;
                            case 4:
                                st3.setBackgroundResource(R.drawable.ic_color_amarillo);
                                break;
                        }
                    }
                    if (dato4!=null){
                        int ico4 = dato4.getInt("icono");
                        int col4 = dato4.getInt("color");
                        switch (ico4) {
                            case 1:
                                Glide.with(this).load(R.drawable.adjunto_ic_imagen_on).into(st4);
                                break;
                            case 2:
                                Glide.with(this).load(R.drawable.adjunto_ic_foto_on).into(st4);
                                break;
                            case 3:
                                Glide.with(this).load(R.drawable.adjunto_ic_video_on).into(st4);
                                break;
                            case 4:
                                Glide.with(this).load(R.drawable.adjunto_ic_audio_on).into(st4);
                                break;
                            case 5:
                                Glide.with(this).load(R.drawable.adjunto_ic_archivo_on).into(st4);
                                break;
                        }
                        switch (col4) {
                            case 1:
                                st4.setBackgroundResource(R.drawable.ic_color_rojo);
                                break;
                            case 2:
                                st4.setBackgroundResource(R.drawable.ic_color_verde);
                                break;
                            case 3:
                                st4.setBackgroundResource(R.drawable.ic_color_violeta);
                                break;
                            case 4:
                                st4.setBackgroundResource(R.drawable.ic_color_amarillo);
                                break;
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                if (User.logged(this)) {User.clear(this);}
                //LoginManager.getInstance().logOut();
                Intent intent = new Intent(this, LogIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
