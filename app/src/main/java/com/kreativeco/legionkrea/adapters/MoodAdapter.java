package com.kreativeco.legionkrea.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.kreativeco.legionkrea.MoodBoard;
import com.kreativeco.legionkrea.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sagamagus on 09/10/17.
 */

public class MoodAdapter extends RecyclerView.Adapter<MoodAdapter.RankViewHolder> {
    JSONArray data;
    Activity mActivity;

public class RankViewHolder extends RecyclerView.ViewHolder{

    int id;
    String nombre;
    JSONObject actual;
    String imagen;

    TextView txtNombre;
    ImageView btnVer;

    public RankViewHolder(View itemView) {
        super(itemView);
        btnVer = (ImageView) itemView.findViewById(R.id.imagen_mood);
        txtNombre = (TextView) itemView.findViewById(R.id.txt_mood);
    }

}

    public MoodAdapter(JSONArray data, Activity activity){
        Log.e("entro al adapter ","hay datos");
        this.data = data;
        mActivity = activity;
    }

    @Override
    public MoodAdapter.RankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rankView;
        rankView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_mood, parent, false);
        return new MoodAdapter.RankViewHolder(rankView);
    }

    @Override
    public void onBindViewHolder(final MoodAdapter.RankViewHolder holder, int position) {

        try {
            //   holder.idReference = data.getJSONObject(position).getInt("id");
            holder.id = data.getJSONObject(position).getInt("id");
            holder.nombre = data.getJSONObject(position).getString("nombre");
            String cam = data.getJSONObject(position).getString("camara");
            String img = data.getJSONObject(position).getString("imagen");
            holder.actual = data.getJSONObject(position);
            holder.txtNombre.setText(holder.nombre);
            holder.imagen="";
            if (!cam.equals("")){
                holder.imagen = cam;
            }else if(!img.equals("")){
                holder.imagen = img;
            }
            Glide.with(mActivity).load(holder.imagen).apply(new RequestOptions().centerCrop()).into(holder.btnVer);
            holder.txtNombre.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MoodBoard)mActivity).loadDMood(holder.actual);
                }
            });
            holder.btnVer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MoodBoard)mActivity).loadDMood(holder.actual);
                }
            });

        } catch (JSONException e) {

            new AlertDialog.Builder(mActivity).setTitle(R.string.txt_error).setMessage(R.string.error_exception).setNeutralButton(R.string.btn_close, null).show();

            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }
}