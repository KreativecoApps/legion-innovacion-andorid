package com.kreativeco.legionkrea.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kreativeco.legionkrea.Proyects;
import com.kreativeco.legionkrea.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sagamagus on 09/10/17.
 */

public class ProyectAdapter extends RecyclerView.Adapter<ProyectAdapter.RankViewHolder> {
    JSONArray data;
    Activity mActivity;

public class RankViewHolder extends RecyclerView.ViewHolder{

    int id;
    String nombre;
    JSONObject actual;

    TextView txtNombre;
    TextView btnVer;

    public RankViewHolder(View itemView) {
        super(itemView);
        btnVer = (TextView) itemView.findViewById(R.id.btn_ver);
        txtNombre = (TextView) itemView.findViewById(R.id.txt_title);
    }

}

    public ProyectAdapter(JSONArray data, Activity activity){
        Log.e("entro al adapter ","hay datos");
        this.data = data;
        mActivity = activity;
    }

    @Override
    public ProyectAdapter.RankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rankView;
        rankView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_proyect, parent, false);
        return new ProyectAdapter.RankViewHolder(rankView);
    }

    @Override
    public void onBindViewHolder(final ProyectAdapter.RankViewHolder holder, int position) {

        try {
            //   holder.idReference = data.getJSONObject(position).getInt("id");
            holder.id = data.getJSONObject(position).getInt("id");
            holder.nombre = data.getJSONObject(position).getString("nombre");
            holder.actual = data.getJSONObject(position);
            holder.txtNombre.setText(holder.nombre);
            holder.btnVer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((Proyects)mActivity).loadDetalle(holder.id);
                }
            });

        } catch (JSONException e) {

            new AlertDialog.Builder(mActivity).setTitle(R.string.txt_error).setMessage(R.string.error_exception).setNeutralButton(R.string.btn_close, null).show();

            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }
}