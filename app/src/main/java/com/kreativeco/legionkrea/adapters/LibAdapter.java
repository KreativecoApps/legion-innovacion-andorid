package com.kreativeco.legionkrea.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kreativeco.legionkrea.Biblioteca;
import com.kreativeco.legionkrea.R;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Sagamagus on 09/10/17.
 */

public class LibAdapter extends RecyclerView.Adapter<LibAdapter.RankViewHolder> {
    JSONArray data;
    Activity mActivity;

public class RankViewHolder extends RecyclerView.ViewHolder{

    int id;
    String nombre;

    TextView txtNombre;


    public RankViewHolder(View itemView) {
        super(itemView);
        txtNombre = (TextView) itemView.findViewById(R.id.txt_title);

    }

}

    public LibAdapter(JSONArray data, Activity activity){
        Log.e("entro al adapter ","hay datos");
        this.data = data;
        mActivity = activity;
    }

    @Override
    public LibAdapter.RankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rankView;
        rankView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_biblioteca, parent, false);
        return new LibAdapter.RankViewHolder(rankView);
    }

    @Override
    public void onBindViewHolder(final LibAdapter.RankViewHolder holder, int position) {

        try {
            //   holder.idReference = data.getJSONObject(position).getInt("id");
            holder.id = data.getJSONObject(position).getInt("id");
            holder.nombre = data.getJSONObject(position).getString("nombre");
            holder.txtNombre.setText(holder.nombre);
            holder.txtNombre.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((Biblioteca)mActivity).DetLibrary(holder.id);
                }
            });

        } catch (JSONException e) {

            new AlertDialog.Builder(mActivity).setTitle(R.string.txt_error).setMessage(R.string.error_exception).setNeutralButton(R.string.btn_close, null).show();

            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }
}