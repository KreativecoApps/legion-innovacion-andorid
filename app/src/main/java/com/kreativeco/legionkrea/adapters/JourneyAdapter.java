package com.kreativeco.legionkrea.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kreativeco.legionkrea.Empatizar;
import com.kreativeco.legionkrea.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sagamagus on 09/10/17.
 */

public class JourneyAdapter extends RecyclerView.Adapter<JourneyAdapter.RankViewHolder> {
    JSONArray data;
    Activity mActivity;

public class RankViewHolder extends RecyclerView.ViewHolder{

    int id, icono, color, emoticon;
    String nombre;
    JSONObject actual;

    ImageView ic1, ic2, ic3, ic4, ic5;

    public RankViewHolder(View itemView) {
        super(itemView);
        ic1 = (ImageView) itemView.findViewById(R.id.ic1);
        ic2 = (ImageView) itemView.findViewById(R.id.ic2);
        ic3 = (ImageView) itemView.findViewById(R.id.ic3);
        ic4 = (ImageView) itemView.findViewById(R.id.ic4);
        ic5 = (ImageView) itemView.findViewById(R.id.ic5);
    }

}

    public JourneyAdapter(JSONArray data, Activity activity){
        Log.e("entro al adapter ","hay datos");
        this.data = data;
        mActivity = activity;
    }

    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        if(position==0) {
            return 0;
        }
        else{
            return 1;
        }
    }

    @Override
    public JourneyAdapter.RankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rankView;
        switch (viewType){
            case 0:
                rankView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_journey_first, parent, false);
                break;
            default:
                rankView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_journey, parent, false);
                break;
        }
        return new JourneyAdapter.RankViewHolder(rankView);
    }

    @Override
    public void onBindViewHolder(final JourneyAdapter.RankViewHolder holder, int position) {

        try {
            if (holder.getItemViewType()==1) {
                //   holder.idReference = data.getJSONObject(position).getInt("id");
                holder.id = data.getJSONObject(position).getInt("id");
                holder.nombre = data.getJSONObject(position).getString("nombre_proyecto");
                holder.actual = data.getJSONObject(position);
                holder.icono = data.getJSONObject(position).getInt("icono");
                holder.color = data.getJSONObject(position).getInt("color");
                holder.emoticon = data.getJSONObject(position).getInt("emoticon");
                switch (holder.emoticon) {
                    case 1:
                        holder.ic1.setVisibility(View.VISIBLE);
                        holder.ic1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ((Empatizar) mActivity).loadNFunction(holder.id, holder.actual);
                            }
                        });
                        break;
                    case 2:
                        holder.ic2.setVisibility(View.VISIBLE);
                        holder.ic2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ((Empatizar) mActivity).loadNFunction(holder.id, holder.actual);
                            }
                        });
                        break;
                    case 3:
                        holder.ic3.setVisibility(View.VISIBLE);
                        holder.ic3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ((Empatizar) mActivity).loadNFunction(holder.id, holder.actual);
                            }
                        });
                        break;
                    case 4:
                        holder.ic4.setVisibility(View.VISIBLE);
                        holder.ic4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ((Empatizar) mActivity).loadNFunction(holder.id, holder.actual);
                            }
                        });
                        break;
                    case 5:
                        holder.ic5.setVisibility(View.VISIBLE);
                        holder.ic5.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ((Empatizar) mActivity).loadNFunction(holder.id, holder.actual);
                            }
                        });
                        break;
                }
                switch (holder.icono) {
                    case 1:
                        holder.ic1.setImageResource(R.drawable.adjunto_ic_imagen_on);
                        holder.ic2.setImageResource(R.drawable.adjunto_ic_imagen_on);
                        holder.ic3.setImageResource(R.drawable.adjunto_ic_imagen_on);
                        holder.ic4.setImageResource(R.drawable.adjunto_ic_imagen_on);
                        holder.ic5.setImageResource(R.drawable.adjunto_ic_imagen_on);
                        break;
                    case 2:
                        holder.ic1.setImageResource(R.drawable.adjunto_ic_foto_on);
                        holder.ic2.setImageResource(R.drawable.adjunto_ic_foto_on);
                        holder.ic3.setImageResource(R.drawable.adjunto_ic_foto_on);
                        holder.ic4.setImageResource(R.drawable.adjunto_ic_foto_on);
                        holder.ic5.setImageResource(R.drawable.adjunto_ic_foto_on);
                        break;
                    case 3:
                        holder.ic1.setImageResource(R.drawable.adjunto_ic_video_on);
                        holder.ic2.setImageResource(R.drawable.adjunto_ic_video_on);
                        holder.ic3.setImageResource(R.drawable.adjunto_ic_video_on);
                        holder.ic4.setImageResource(R.drawable.adjunto_ic_video_on);
                        holder.ic5.setImageResource(R.drawable.adjunto_ic_video_on);
                        break;
                    case 4:
                        holder.ic1.setImageResource(R.drawable.adjunto_ic_audio_on);
                        holder.ic2.setImageResource(R.drawable.adjunto_ic_audio_on);
                        holder.ic3.setImageResource(R.drawable.adjunto_ic_audio_on);
                        holder.ic4.setImageResource(R.drawable.adjunto_ic_audio_on);
                        holder.ic5.setImageResource(R.drawable.adjunto_ic_audio_on);
                        break;
                    case 5:
                        holder.ic1.setImageResource(R.drawable.adjunto_ic_archivo_on);
                        holder.ic2.setImageResource(R.drawable.adjunto_ic_archivo_on);
                        holder.ic3.setImageResource(R.drawable.adjunto_ic_archivo_on);
                        holder.ic4.setImageResource(R.drawable.adjunto_ic_archivo_on);
                        holder.ic5.setImageResource(R.drawable.adjunto_ic_archivo_on);
                        break;
                }
                switch (holder.color) {
                    case 1:
                        holder.ic1.setBackgroundResource(R.drawable.ic_color_rojo);
                        holder.ic2.setBackgroundResource(R.drawable.ic_color_rojo);
                        holder.ic3.setBackgroundResource(R.drawable.ic_color_rojo);
                        holder.ic4.setBackgroundResource(R.drawable.ic_color_rojo);
                        holder.ic5.setBackgroundResource(R.drawable.ic_color_rojo);
                        break;
                    case 2:
                        holder.ic1.setBackgroundResource(R.drawable.ic_color_verde);
                        holder.ic2.setBackgroundResource(R.drawable.ic_color_verde);
                        holder.ic3.setBackgroundResource(R.drawable.ic_color_verde);
                        holder.ic4.setBackgroundResource(R.drawable.ic_color_verde);
                        holder.ic5.setBackgroundResource(R.drawable.ic_color_verde);
                        break;
                    case 3:
                        holder.ic1.setBackgroundResource(R.drawable.ic_color_violeta);
                        holder.ic2.setBackgroundResource(R.drawable.ic_color_violeta);
                        holder.ic3.setBackgroundResource(R.drawable.ic_color_violeta);
                        holder.ic4.setBackgroundResource(R.drawable.ic_color_violeta);
                        holder.ic5.setBackgroundResource(R.drawable.ic_color_violeta);
                        break;
                    case 4:
                        holder.ic1.setBackgroundResource(R.drawable.ic_color_amarillo);
                        holder.ic2.setBackgroundResource(R.drawable.ic_color_amarillo);
                        holder.ic3.setBackgroundResource(R.drawable.ic_color_amarillo);
                        holder.ic4.setBackgroundResource(R.drawable.ic_color_amarillo);
                        holder.ic5.setBackgroundResource(R.drawable.ic_color_amarillo);
                        break;
                }
            }

        } catch (JSONException e) {

            new AlertDialog.Builder(mActivity).setTitle(R.string.txt_error).setMessage(R.string.error_exception).setNeutralButton(R.string.btn_close, null).show();

            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }
}