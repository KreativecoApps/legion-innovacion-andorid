package com.kreativeco.legionkrea.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kreativeco.legionkrea.R;
import com.kreativeco.legionkrea.Testing;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sagamagus on 09/10/17.
 */

public class TaskPendAdapter extends RecyclerView.Adapter<TaskPendAdapter.RankViewHolder> {
    JSONArray data;
    Activity mActivity;

public class RankViewHolder extends RecyclerView.ViewHolder{

    JSONObject actual;

    TextView btnVer;
    TextView title;
    TextView fecha;
    int id;

    public RankViewHolder(View itemView) {
        super(itemView);
        btnVer = (TextView) itemView.findViewById(R.id.txt_prev);
        fecha = (TextView) itemView.findViewById(R.id.txt_fecha);
        title = (TextView) itemView.findViewById(R.id.txt_titleC);
    }

}

    public TaskPendAdapter(JSONArray data, Activity activity){
        Log.e("entro al adapter ","hay datos");
        this.data = data;
        mActivity = activity;
    }

    @Override
    public TaskPendAdapter.RankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rankView;
        rankView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_testing2, parent, false);
        return new TaskPendAdapter.RankViewHolder(rankView);
    }

    @Override
    public void onBindViewHolder(final TaskPendAdapter.RankViewHolder holder, int position) {

        try {
            //   holder.idReference = data.getJSONObject(position).getInt("id");
            holder.actual = data.getJSONObject(position);
            holder.btnVer.setText(holder.actual.getString("descripcion"));
            holder.title.setText(holder.actual.getString("nombre"));
            holder.fecha.setText(holder.actual.getString("fecha"));
            holder.id=holder.actual.getInt("id");
            holder.title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((Testing)mActivity).completeTask(holder.id);
                }
            });


        } catch (JSONException e) {

            new AlertDialog.Builder(mActivity).setTitle(R.string.txt_error).setMessage(R.string.error_exception).setNeutralButton(R.string.btn_close, null).show();

            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }
}