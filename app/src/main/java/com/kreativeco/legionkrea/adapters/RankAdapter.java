package com.kreativeco.legionkrea.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kreativeco.legionkrea.R;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Sagamagus on 09/10/17.
 */

public class RankAdapter extends RecyclerView.Adapter<RankAdapter.RankViewHolder> {
    JSONArray data;
    Activity mActivity;

public class RankViewHolder extends RecyclerView.ViewHolder{

    int id;
    int rank;
    String nombre;
    String email;
    String imagen;
    int puntos;

    TextView txtNombre;
    TextView txtDesc;
    TextView txtNumber;
    ImageView foto;

    public RankViewHolder(View itemView) {
        super(itemView);
        txtNombre = (TextView) itemView.findViewById(R.id.txt_title);
        txtDesc = (TextView) itemView.findViewById(R.id.txt_puntos);
        txtNumber = (TextView) itemView.findViewById(R.id.txt_number);
        foto = (ImageView) itemView.findViewById(R.id.img_foto);
    }

}

    public RankAdapter(JSONArray data, Activity activity){
        Log.e("entro al adapter ","hay datos");
        this.data = data;
        mActivity = activity;
    }

    @Override
    public RankAdapter.RankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rankView;
        rankView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_ranking, parent, false);
        return new RankAdapter.RankViewHolder(rankView);
    }

    @Override
    public void onBindViewHolder(RankAdapter.RankViewHolder holder, int position) {

        try {
            //   holder.idReference = data.getJSONObject(position).getInt("id");
            holder.id = data.getJSONObject(position).getInt("id");
            holder.rank = data.getJSONObject(position).getInt("rank");
            holder.nombre = data.getJSONObject(position).getString("nombre");
            holder.email = data.getJSONObject(position).getString("email");
            holder.imagen = data.getJSONObject(position).getString("imagen");
            holder.puntos = data.getJSONObject(position).getInt("puntos");
            holder.txtNombre.setText(holder.nombre);
            String r=holder.rank+"";
            holder.txtNumber.setText(r);
            String descripcion = holder.puntos+" PUNTOS";
            holder.txtDesc.setText(descripcion);
            if(!holder.imagen.equals("none")&&!holder.imagen.equals("")&&!holder.imagen.equals("default")){
                Glide.with(mActivity).load(holder.imagen).into(holder.foto);
            }

        } catch (JSONException e) {

            new AlertDialog.Builder(mActivity).setTitle(R.string.txt_error).setMessage(R.string.error_exception).setNeutralButton(R.string.btn_close, null).show();

            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }
}