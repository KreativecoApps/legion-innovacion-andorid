package com.kreativeco.legionkrea.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.kreativeco.legionkrea.NuevoStoryboard;
import com.kreativeco.legionkrea.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sagamagus on 09/10/17.
 */

public class StoryListAdapter extends RecyclerView.Adapter<StoryListAdapter.RankViewHolder> {
    JSONArray data;
    Activity mActivity;

public class RankViewHolder extends RecyclerView.ViewHolder{

    JSONObject actual;

    ImageView btnVer;

    public RankViewHolder(View itemView) {
        super(itemView);
        btnVer = (ImageView) itemView.findViewById(R.id.imagen_story);
    }

}

    public StoryListAdapter(JSONArray data, Activity activity){
        Log.e("entro al adapter ","hay datos");
        this.data = data;
        mActivity = activity;
    }

    @Override
    public StoryListAdapter.RankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rankView;
        rankView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_story_list, parent, false);
        return new StoryListAdapter.RankViewHolder(rankView);
    }

    @Override
    public void onBindViewHolder(final StoryListAdapter.RankViewHolder holder, int position) {

        try {
            //   holder.idReference = data.getJSONObject(position).getInt("id");
            holder.actual = data.getJSONObject(position);
            if (position==data.length()-1){Glide.with(mActivity).load(R.drawable.storyboard_nuevo).into(holder.btnVer);}
            holder.btnVer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((NuevoStoryboard)mActivity).loadStDet(holder.actual);
                }
            });

        } catch (JSONException e) {

            new AlertDialog.Builder(mActivity).setTitle(R.string.txt_error).setMessage(R.string.error_exception).setNeutralButton(R.string.btn_close, null).show();

            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }
}