package com.kreativeco.legionkrea.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kreativeco.legionkrea.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Random;

/**
 * Created by Sagamagus on 09/10/17.
 */

public class NotAdapter extends RecyclerView.Adapter<NotAdapter.RankViewHolder> {
    JSONArray data;
    Activity mActivity;

public class RankViewHolder extends RecyclerView.ViewHolder{

    int id;
    String nombre;
    int colors[] ={R.color.colorLegionAqua, R.color.colorLegionGray, R.color.colorLegionMorado,R.color.colorLegionRed,R.color.colorYellow};

    TextView txtNombre;
    TextView txtDesc;
    ImageView foto;

    public RankViewHolder(View itemView) {
        super(itemView);
        txtNombre = (TextView) itemView.findViewById(R.id.txt_title);
        txtDesc = (TextView) itemView.findViewById(R.id.txt_puntos);
        foto = (ImageView) itemView.findViewById(R.id.img_foto);
    }

}

    public NotAdapter(JSONArray data, Activity activity){
        Log.e("entro al adapter ","hay datos");
        this.data = data;
        mActivity = activity;
    }

    @Override
    public NotAdapter.RankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rankView;
        rankView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_notif, parent, false);
        return new NotAdapter.RankViewHolder(rankView);
    }

    @Override
    public void onBindViewHolder(NotAdapter.RankViewHolder holder, int position) {

        try {
            //   holder.idReference = data.getJSONObject(position).getInt("id");
            holder.id = data.getJSONObject(position).getInt("id_notificacion_user");
            holder.nombre = data.getJSONObject(position).getString("notificacion");
            holder.txtNombre.setText(holder.nombre);
            //holder.txtDesc.setText(fecha);
            holder.foto.setBackgroundResource(holder.colors[new Random().nextInt(holder.colors.length)]);

        } catch (JSONException e) {

            new AlertDialog.Builder(mActivity).setTitle(R.string.txt_error).setMessage(R.string.error_exception).setNeutralButton(R.string.btn_close, null).show();

            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }
}