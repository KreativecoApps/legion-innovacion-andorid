package com.kreativeco.legionkrea.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kreativeco.legionkrea.Biblioteca;
import com.kreativeco.legionkrea.R;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Sagamagus on 09/10/17.
 */

public class LibDetAdapter extends RecyclerView.Adapter<LibDetAdapter.RankViewHolder> {
    JSONArray data;
    Activity mActivity;

public class RankViewHolder extends RecyclerView.ViewHolder{

    int id;
    String nombre;
    String file;
    String type;

    TextView txtNombre;

    public RankViewHolder(View itemView) {
        super(itemView);
        txtNombre = (TextView) itemView.findViewById(R.id.btn_ver);
    }

}

    public LibDetAdapter(JSONArray data, Activity activity){
        Log.e("entro al adapter ","hay datos");
        this.data = data;
        mActivity = activity;
    }

    @Override
    public LibDetAdapter.RankViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rankView;
        rankView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_download, parent, false);
        return new LibDetAdapter.RankViewHolder(rankView);
    }

    @Override
    public void onBindViewHolder(final LibDetAdapter.RankViewHolder holder, int position) {

        try {
            //   holder.idReference = data.getJSONObject(position).getInt("id");
            holder.id = data.getJSONObject(position).getInt("id");
            holder.nombre = data.getJSONObject(position).getString("nombre_file");
            holder.file = data.getJSONObject(position).getString("file");
            holder.type = data.getJSONObject(position).getString("tipo_archivo");
            holder.txtNombre.setText(holder.nombre);
            if (holder.type.equals("Video")){
                holder.txtNombre.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((Biblioteca)mActivity).DetLibraryLoadVideo(holder.id,holder.file);
                    }
                });
            }
            else{//if (holder.type.equals("PDF")){
                holder.txtNombre.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((Biblioteca)mActivity).DetLibraryLoadPDF(holder.id,holder.file,holder.type);
                    }
                });
            }


        } catch (JSONException e) {

            new AlertDialog.Builder(mActivity).setTitle(R.string.txt_error).setMessage(R.string.error_exception).setNeutralButton(R.string.btn_close, null).show();

            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return data.length();
    }
}