package com.kreativeco.legionkrea;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class NuevaTarea extends Activity implements WebBridge.WebBridgeListener{

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    private EditText title, body;
    private TextView fecha;
    private int year, month, day, proyect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nueva_tarea);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        title = (EditText) findViewById(R.id.edt_title);
        body = (EditText) findViewById(R.id.edt_body);
        fecha = (TextView) findViewById(R.id.ctr_fecha);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        String ty = formattedDate.substring(0,4);
        String tm = formattedDate.substring(5,7);
        String td = formattedDate.substring(8,10);
        Log.e("fecha:",ty+tm+td);
        year = Integer.parseInt(ty);
        month = Integer.parseInt(tm);
        day = Integer.parseInt(td);
        proyect = getIntent().getIntExtra("proyect",0);
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("NuevaTarea_Activity", bundle);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Nueva Tarea",this.getClass().getSimpleName());
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }
    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }
    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }
    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }
    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }
    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "Selecciona una fecha de entrega",Toast.LENGTH_SHORT).show();

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = year
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2+1, arg3);
                }
            };

    private void showDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
        String d, m;
        if (month<10){m="0"+month;}else{m=""+month;}
        if (day<10){d="0"+day;}else{d=""+day;}

        fecha.setText(new StringBuilder().append(year).append("-")
                .append(m).append("-").append(d));
    }

    public void send(View view){
        if (proyect!=0) {
            ArrayList<String> errors = new ArrayList<String>();
            if (title.length() < 1) errors.add(getString(R.string.edit_text_error_titulo));
            if (body.length() < 1) errors.add(getString(R.string.edit_text_error_body));
            if (fecha.length() < 1) errors.add("Debe seleccionar una fecha para la tarea");
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = df.format(c.getTime());
            String ty = formattedDate.substring(0, 4);
            String tm = formattedDate.substring(5, 7);
            String td = formattedDate.substring(8, 10);
            Log.e("fecha:", ty + tm + td);
            int ny = Integer.parseInt(ty);
            int nm = Integer.parseInt(tm);
            int nd = Integer.parseInt(td);

            if (year < ny || month < nm || day < nd)
                errors.add("Debe seleccionar la fecha de hoy o posterior para la tarea");

            if (errors.size() != 0) {
                String msg = "";
                for (String s : errors) {
                    msg += "- " + s + "\n";
                }
                new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(msg.trim()).setNeutralButton(R.string.btn_close, null).show();
                return;
            }
            HashMap<String, Object> params = new HashMap<>();
            params.put("token", User.getToken(this));
            params.put("id_proyecto",proyect);
            params.put("nombre", title.getText().toString());
            params.put("descripcion", body.getText().toString());
            params.put("fecha", fecha.getText().toString());
            Log.e("ES este:", params.toString());
            WebBridge.send("guardarretrosolevaluacion=1&key=admin", params, "Enviando", this, this);
        }else{
            finish();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")){
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle("Aviso")
                        .setMessage(json.getString("msg"))
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }

                        })
                        .show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                if (User.logged(this)) {User.clear(this);}
                //LoginManager.getInstance().logOut();
                Intent intent = new Intent(this, LogIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
