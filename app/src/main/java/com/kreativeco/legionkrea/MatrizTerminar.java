package com.kreativeco.legionkrea;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MatrizTerminar extends Activity implements WebBridge.WebBridgeListener{

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    private int proyect, matriz, submatriz, carta, stars;
    private TextView title;
    private String body;
    private LinearLayout card1, card2, card3, card4;
    private ImageView s1, s2, s3, s4, s5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.matriz_terminar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        title = (TextView) findViewById(R.id.edt_title);
        card1 = (LinearLayout) findViewById(R.id.lny_cards);
        card2 = (LinearLayout) findViewById(R.id.lny_cards2);
        card3 = (LinearLayout) findViewById(R.id.lny_cards3);
        card4 = (LinearLayout) findViewById(R.id.lny_cards4);
        s1 = (ImageView) findViewById(R.id.star1);
        s2 = (ImageView) findViewById(R.id.star2);
        s3 = (ImageView) findViewById(R.id.star3);
        s4 = (ImageView) findViewById(R.id.star4);
        s5 = (ImageView) findViewById(R.id.star5);
        card1.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
        card2.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
        card3.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
        card4.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
        matriz= getIntent().getExtras().getInt("matriz",0);
        proyect= getIntent().getExtras().getInt("proyect",0);
        submatriz= getIntent().getExtras().getInt("submatriz",0);
        carta= getIntent().getExtras().getInt("carta",0);
        stars= getIntent().getExtras().getInt("stars",0);
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("MAtrizTerminar_Activity", bundle);
        switch (stars){
            case 1:
                s1.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s2.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                s3.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                s4.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                s5.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                break;
            case 2:
                s1.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s2.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s3.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                s4.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                s5.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                break;
            case 3:
                s1.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s2.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s3.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s4.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                s5.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                break;
            case 4:
                s1.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s2.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s3.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s4.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s5.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                break;
            case 5:
                s1.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s2.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s3.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s4.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s5.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                break;

        }
        body = getIntent().getExtras().getString("nota");
        String tit = getIntent().getExtras().getString("title");
        title.setText(tit);
        for (int i = 0; i < card1.getChildCount(); i++) {
            View v = card1.getChildAt(i);
            if (v instanceof TextView) {
                if (carta==1) {
                    ((TextView) v).setTextColor(Color.WHITE);
                } else {
                    ((TextView) v).setTextColor(Color.BLACK);
                }
            }

        }
        for (int i = 0; i < card2.getChildCount(); i++) {
            View v = card2.getChildAt(i);
            if (v instanceof TextView) {
                if (carta==2) {
                    ((TextView) v).setTextColor(Color.WHITE);
                } else {
                    ((TextView) v).setTextColor(Color.BLACK);
                }
            }
        }
        for (int i = 0; i < card3.getChildCount(); i++) {
            View v = card3.getChildAt(i);
            if (v instanceof TextView) {
                if (carta==3) {
                    ((TextView) v).setTextColor(Color.WHITE);
                } else {
                    ((TextView) v).setTextColor(Color.BLACK);
                }
            }
        }
        for (int i = 0; i < card4.getChildCount(); i++) {
            View v = card4.getChildAt(i);
            if (v instanceof TextView) {
                if (carta==4) {
                    ((TextView) v).setTextColor(Color.WHITE);
                } else {
                    ((TextView) v).setTextColor(Color.BLACK);
                }
            }
        }

        switch (carta){
            case 1:
                card1.setBackgroundResource(R.drawable.definir_matriz_opc_color);
                break;

            case 2:
                card2.setBackgroundResource(R.drawable.definir_matriz_opc_color);
                break;

            case 3:
                card3.setBackgroundResource(R.drawable.definir_matriz_opc_color);
                break;

            case 4:
                card4.setBackgroundResource(R.drawable.definir_matriz_opc_color);
                break;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Matriz Terminar",this.getClass().getSimpleName());
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }

    public void loadDefinir(View view){
        Intent loginIntent = new Intent(this, Definir.class);
        startActivity(loginIntent);
    }

    public void loadNProyect(View view){
        Intent loginIntent = new Intent(this, NewProyect.class);
        startActivity(loginIntent);
    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }

    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }

    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }

    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    public void setStars(View view) {
        stars = Integer.parseInt(view.getTag().toString());
        switch (stars){
            case 1:
                s1.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s2.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                s3.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                s4.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                s5.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                break;
            case 2:
                s1.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s2.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s3.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                s4.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                s5.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                break;
            case 3:
                s1.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s2.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s3.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s4.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                s5.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                break;
            case 4:
                s1.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s2.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s3.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s4.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s5.setBackgroundResource(R.drawable.evaluar_ic_estrella_off);
                break;
            case 5:
                s1.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s2.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s3.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s4.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                s5.setBackgroundResource(R.drawable.evaluar_ic_estrella_on);
                break;

        }
    }

    public void send(View view) {

        ArrayList<String> errors = new ArrayList<String>();
        if (proyect==0) errors.add(getString(R.string.edttxt_error_proyect));
        if (matriz==0) errors.add(getString(R.string.edttxt_error_matriz));
        if (submatriz==0) errors.add(getString(R.string.edttxt_error_submatriz));
        if (stars==0) errors.add(getString(R.string.edttxt_error_stars));
        if (body.length() < 1) errors.add(getString(R.string.edttxt_error_body));
        if (errors.size() != 0) {
            String msg = "";
            for (String s : errors) {
                msg += "- " + s + "\n";
            }
            new android.app.AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(msg.trim()).setNeutralButton(R.string.btn_close, null).show();
            return;
        }

        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id_proyecto",proyect);
        params.put("id_matriz",matriz);
        params.put("id_submatriz", submatriz);
        params.put("nota",body);
        params.put("estrellas", stars);
        Log.e("---->",params.toString());
        WebBridge.send("guardarproyectosubmatriz=1&key=admin", params, "Enviando", this, this);


    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")){
                new android.app.AlertDialog.Builder(MatrizTerminar.this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle("Aviso")
                        .setMessage(json.getString("message"))
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }

                        })
                        .show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                if (User.logged(this)) {User.clear(this);}
                //LoginManager.getInstance().logOut();
                Intent intent = new Intent(this, LogIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
