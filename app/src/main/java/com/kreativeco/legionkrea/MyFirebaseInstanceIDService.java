package com.kreativeco.legionkrea;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.kreativeco.legionkrea.utils.User;


/**
 * Created by eynarfernandooteromedina on 09/03/17.
 */

//Class extending FirebaseInstanceIdService
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {


    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        try {

            //Getting registration token
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();

            //Displaying token on logcat
            Log.d(TAG, "Refreshed token: " + refreshedToken);

            User.setC("fire", refreshedToken, getBaseContext());

        } catch (Exception e) {


        }


    }

}