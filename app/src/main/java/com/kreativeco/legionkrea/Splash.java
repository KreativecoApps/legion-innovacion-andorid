package com.kreativeco.legionkrea;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.kreativeco.legionkrea.utils.User;

import io.fabric.sdk.android.Fabric;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.splash);

        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{

                    if (User.logged(Splash.this)) {

                        Intent homeIntent = new Intent(Splash.this, Home.class);
                        startActivity(homeIntent);

                    } else {

                        Intent loginIntent = new Intent(Splash.this, LogIn.class);
                        startActivity(loginIntent);

                    }

                    finish();

                }
            }
        };

        timerThread.start();
    }
}
