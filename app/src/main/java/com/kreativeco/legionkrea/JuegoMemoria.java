package com.kreativeco.legionkrea;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class JuegoMemoria extends AppCompatActivity implements WebBridge.WebBridgeListener{

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    CountDownTimer cTimer = null;
    private int etapa, grupo, time;
    private JSONArray data;
    private String oldIDs = "";
    private String newIDs = "";
    private Boolean control = true;
    private TextView txtTime;
    List<Integer> solution;
    List<Integer> ids;
    List<Integer> idsN;
    private ImageView[] imagenes = new ImageView[10];
    private int[] cajas = {R.id.imageView, R.id.imageView2, R.id.imageView3, R.id.imageView4, R.id.imageView5, R.id.imageView6, R.id.imageView7, R.id.imageView8, R.id.imageView9, R.id.imageView10};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.juego_m);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        solution = new ArrayList<>();
        ids = new ArrayList<>();
        idsN = new ArrayList<>();
        for(int i=0;i<10;i++) {
            solution.add(cajas[i]);
        }
        cleanCells();
        txtTime = (TextView) findViewById(R.id.txt_time);
        //loadGame();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("Memoria_Activity", bundle);
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        WebBridge.send("listadomemoria=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Juego de Memoria",this.getClass().getSimpleName());
    }

    protected void onDestroy() {
        super.onDestroy();
        cancelTimer();
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }

    public void closeSesion (View view){
        Log.e("salio","primero");
        closeDrawer(mDrawer);
        if (User.logged(this)) {
            User.clear(this);}
        //LoginManager.getInstance().logOut();
        Intent intent = new Intent(this, LogIn.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity (intent);
        finish();

    }

    public void loadNProyect(View view){
        Intent loginIntent = new Intent(this, NewProyect.class);
        startActivity(loginIntent);
    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }

    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }

    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }

    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    void cleanCells(){
        Collections.shuffle(solution);
        for(int i=0;i<10;i++) {
            imagenes[i] = (ImageView) findViewById(solution.get(i));
            imagenes[i].setVisibility(View.INVISIBLE);
        }
        ids.clear();
        idsN.clear();
    }

    void loadGame() {
        Log.e("entro->","timer");
        int real = (time*1000)+1000;
        cTimer = new CountDownTimer(real, 1000) {

            public void onTick(long millisUntilFinished) {

                long remainingTime = millisUntilFinished / 1000;

                if (remainingTime > 9) {
                    txtTime.setText("0:" + millisUntilFinished / 1000+"");

                } else {
                    txtTime.setText("0:0" + millisUntilFinished / 1000+"");

                }
            }

            public void onFinish() {
                txtTime.setText("0:00");
                //sendPoints();
                Log.e (data.length()+"",ids.size()+"");
                if (time!=2){
                    sendResponse();
                }else{
                    callNext();
                }
            }
        };
        cTimer.start();
    }

    //cancel timer
    void cancelTimer() {
        if (cTimer != null)
            cTimer.cancel();
    }

    public void garbage(View view){
        ids.add(0);
        view.setVisibility(View.INVISIBLE);
    }


    public void callNext(){
        cancelTimer();
        Log.e("idsO:",oldIDs);
        control = true;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("elemento_anterior",newIDs);
        params.put("grupo", grupo);
        WebBridge.send("listadomemoria=1&key=admin", params, "Enviando", this, this);
    }

    public void restart(){
        Log.e("idsO:",oldIDs);
        control = true;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        WebBridge.send("listadomemoria=1&key=admin", params, "Enviando", this, this);
    }

    public void sendResponse(){
        cancelTimer();
        String sel;
        sel = ids +"";
        sel = sel.replaceAll("\\[","");
        sel = sel.replaceAll("\\]","");
        control = false;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("old_ids",oldIDs);
        params.put("ids_seleccionados", sel);
        params.put("etapa", etapa);
        WebBridge.send("calificamemoria=1&key=admin", params, "Enviando", this, this);
    }

    public  void close(View view){
        finish();
    }

    public  void clickBack(View view){
        finish();
    }

    public void next(View view){
        finish();
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {

        try {
            if (json.getBoolean("success")){
                if (control) {
                    cleanCells();
                    etapa = json.getInt("etapa");
                    grupo = json.getInt("grupo");
                    if (json.has("old_ids")) {
                        oldIDs = json.getString("old_ids");
                    }
                    if (json.has("ids_new")) {
                        newIDs = json.getString("ids_new");
                    }
                    data = json.getJSONArray("data");
                    switch (data.length()) {
                        case 4:
                            time = 3;
                            break;
                        case 6:
                            time = 4;
                            break;
                        case 8:
                            time = 5;
                            break;
                        case 10:
                            time = 6;
                            break;
                        default:
                            time = 2;
                            break;
                    }
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject act = data.getJSONObject(i);
                        imagenes[i].setVisibility(View.VISIBLE);
                        String imag = act.getString("imagen");
                        //Log.e("imagen:", imag);
                        Glide.with(JuegoMemoria.this).load(imag).into(imagenes[i]);
                        Log.e("imagen:", imag);
                        idsN.add(act.getInt("id"));
                        //idsO.add(act.getInt("id"));
                        if (time != 2) {
                            final int finalI = i;
                            imagenes[i].setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ids.add(idsN.get(finalI));
                                    imagenes[finalI].setVisibility(View.INVISIBLE);
                                    if (ids.size() == data.length()-2) {
                                        sendResponse();
                                    }
                                }
                            });
                        }else{
                            imagenes[i].setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                }
                            });
                        }
                    }
                    loadGame();
                }else{
                    cancelTimer();
                    if (json.getJSONObject("data").getString("msg").equals("Has terminado el juegos, has ganado 5 puntos.")){
                        new android.app.AlertDialog.Builder(JuegoMemoria.this)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle("Aviso")
                                .setMessage(json.getJSONObject("data").getString("msg"))
                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        finish();
                                    }
                                })
                                .show();
                    }else {
                        new android.app.AlertDialog.Builder(JuegoMemoria.this)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle("Aviso")
                                .setMessage(json.getJSONObject("data").getString("msg"))
                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        callNext();
                                    }
                                })
                                .show();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                if (User.logged(this)) {User.clear(this);}
                //LoginManager.getInstance().logOut();
                Intent intent = new Intent(this, LogIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }else{
                if (json.getString("error_message").equals("El parametro ids_seleccionados se encuentra vacio.")){
                    new android.app.AlertDialog.Builder(JuegoMemoria.this)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle("Error")
                            .setMessage("Fallaste, pierdes 1 punto")
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    restart();
                                }
                            })
                            .show();
                }else {
                    new android.app.AlertDialog.Builder(JuegoMemoria.this)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle("Error")
                            .setMessage(json.getString("error_message"))
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    restart();
                                }
                            })
                            .show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        cancelTimer();
        finish();
    }
}
