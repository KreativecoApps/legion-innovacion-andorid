package com.kreativeco.legionkrea;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Register extends Activity implements WebBridge.WebBridgeListener{

    public float d;
    EditText email, pass, user;
    TextView txt;
    ImageView foto;
    RelativeLayout container;
    Spinner empresa, rol;
    int rol_id, empresa_id;
    Boolean registrado, cargado, logeado, dispo;
    List<Integer> id_empresas = new ArrayList<Integer>();
    List<Integer> id_roles = new ArrayList<Integer>();
    List<String> empresasArray =  new ArrayList<String>();
    List<String> rolesArray =  new ArrayList<String>();

    private static final int START_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE_RESULT = 0;
    private static int RESULT_LOAD_IMG = 1;
    private static int RESULT_LOAD_FILE = 2;

    private Dialog alert;
    private String strFotoLocation = "";
    private File fileSendPhoto = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        container = (RelativeLayout) findViewById(R.id.container);
        d = getResources().getDisplayMetrics().density;
        View.OnFocusChangeListener foquito = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                LinearLayout r = (LinearLayout)view.getParent();
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)r.getLayoutParams();
                if (b) {
                    r.setBackgroundResource(R.drawable.registro_campo_sombra);
                    params.setMargins((int)(20*d), (int)(-12*d), (int)(20*d), (int)(-12*d));
                    r.setLayoutParams(params);
                    r.invalidate();
                    r.bringToFront();
                    container.invalidate();
                } else {
                    r.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    params.setMargins((int)(50*d), 0, (int)(50*d), 0);
                    r.setLayoutParams(params);
                    r.invalidate();
                }
            }
        };
        registrado = false;
        cargado = false;
        logeado = false;
        dispo = false;
        user = (EditText) findViewById(R.id.edt_user);
        email = (EditText) findViewById(R.id.edt_email);
        pass = (EditText) findViewById(R.id.edt_pass);
        empresa = (Spinner) findViewById(R.id.edt_empresa);
        rol = (Spinner) findViewById(R.id.edt_rol);
        user.setOnFocusChangeListener(foquito);
        email.setOnFocusChangeListener(foquito);
        pass.setOnFocusChangeListener(foquito);
        empresa.setFocusableInTouchMode(true);
        empresa.setFocusable(true);
        rol.setFocusableInTouchMode(true);
        rol.setFocusable(true);
        empresa.setOnFocusChangeListener(foquito);
        rol.setOnFocusChangeListener(foquito);
        txt = (TextView) findViewById(R.id.txt_txt);
        txt.setFocusableInTouchMode(true);
        txt.setFocusable(true);
        foto = (ImageView) findViewById(R.id.txt_title);
        WebBridge.send("listadoempresa=1&key=admin", "Cargando", this, this);
    }

    public void clickCamera(View v) {

        alert = new Dialog(this);
        alert.setTitle(getResources().getString(R.string.txt_select_option));
        alert.setContentView(getLayoutInflater().inflate(R.layout.dialog_alert_photo, null));
        alert.findViewById(R.id.bt_select_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.cancel();
                takePhoto();
            }
        });

        alert.findViewById(R.id.bt_select_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.cancel();
                loadImagefromGallery(v);
            }
        });

        alert.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.cancel();
            }
        });

        alert.show();

    }


    @TargetApi(Build.VERSION_CODES.M)
    private void takePhoto() {

        if (ContextCompat.checkSelfPermission(Register.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            callCameraApp();
        } else {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(Register.this, getString(R.string.txt_request_permissions), Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_EXTERNAL_STORAGE_RESULT);
        }
    }

    private void callCameraApp() {

        Intent callCameraIntent = new Intent();
        if (Build.VERSION.SDK_INT >= 23)
            callCameraIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE_SECURE);
        else callCameraIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri photoURI = null;

        try {
            File photoFile = createImageFile();
            photoURI = FileProvider.getUriForFile(Register.this,
                    getString(R.string.file_provider_authority),
                    photoFile);
        } catch (Exception e) {
            Log.e("EXCEPTION", e.toString());
        }

        callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        startActivityForResult(callCameraIntent, START_CAMERA);

    }

    File createImageFile() throws IOException {

        String strDate = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String nameImage = "image_" + strDate + "_";

        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(nameImage, ".jpg", storageDir);
        strFotoLocation = image.getAbsolutePath();

        return image;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void loadImagefromGallery(View view) {
        // Create intent to Open Image applications like Gallery, Google Photos
        if (ContextCompat.checkSelfPermission(Register.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            // Start the Intent
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        } else {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(Register.this, getString(R.string.txt_request_permissions), Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_EXTERNAL_STORAGE_RESULT);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                // Get the Image from data
                Log.e("galeria","entro");
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                strFotoLocation = cursor.getString(columnIndex);

                cursor.close();

                // Set the Image in ImageView after decoding the String
                savePhoto(strFotoLocation);

            }else if (requestCode == START_CAMERA && resultCode == RESULT_OK) {
                Log.e("camara","entro");
                savePhoto(strFotoLocation);

            }else {
                Toast.makeText(this, "No haz seleccionado una imagen", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {

            if (strFotoLocation.equals(""))Toast.makeText(this, "Ocurrió algún error", Toast.LENGTH_LONG).show();
        }

    }

    public void savePhoto(String image) {
        fileSendPhoto = new File(image);
        /*HashMap<String, Object> params = new HashMap<>();
        params.put("foto", fileSendPhoto);
        params.put("token", User.getToken(this));*/
        Log.e("foto foto",fileSendPhoto.toString());
        Glide.with(this).load(strFotoLocation).into(foto);
        new android.support.v7.app.AlertDialog.Builder(this).setTitle("Exito").setMessage("Foto subida con exito").setNeutralButton(R.string.btn_close, null).show();
    }

    public void send(View view) {

        ArrayList<String> errors = new ArrayList<String>();
        if (email.getText().length() < 1) errors.add(getString(R.string.edit_text_error_mail));
        if (pass.getText().length() < 1) errors.add(getString(R.string.edit_text_error_pass));
        if (user.getText().length() < 1) errors.add(getString(R.string.edit_text_error_name));
        //if (strFotoLocation.length() < 1) errors.add(getString(R.string.edit_text_error_foto));

        if (errors.size() != 0) {
            String msg = "";
            for (String s : errors) {
                msg += "- " + s + "\n";
            }
            new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(msg.trim()).setNeutralButton(R.string.btn_close, null).show();
            return;
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put("nombre", user.getText());
        params.put("email", email.getText());
        params.put("password", pass.getText());
        if(!strFotoLocation.equals("")){params.put("imagen",fileSendPhoto);}
        params.put("id_empresa",empresa_id);
        params.put("rol",rol_id);
        registrado = true;
        Log.e("---->",params.toString());
        WebBridge.send("register=1&key=admin", params, "Enviando", this, this);


    }

    public void loadRoles(int est){
        HashMap<String, Object> params = new HashMap<>();
        params.put("id_empresa", est);
        WebBridge.send("listadoroles=1&key=admin", params, "Cargando", this, this);
    }

    public void goHome(){
        Intent loginIntent = new Intent(Register.this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        finish();
    }

    public void close(View view){
        finish();
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if(cargado)
            {
                if(registrado)
                {
                    if (dispo){
                        FirebaseAnalytics mFirebaseAnalytics;
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance(Register.this);
                        Bundle bundle = new Bundle();
                        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
                        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
                        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);
                        goHome();
                    }
                    else if(logeado){
                        String token;
                        int id;
                        String correo;
                        String nombre;
                        int rol, emp, func, proy, punt;
                        String rol_n;
                        String foto;
                        JSONObject arr = json.getJSONObject("data");
                        token = arr.getString("token");
                        id = arr.getInt("id_user");
                        correo = arr.getString("email");
                        nombre = arr.getString("nombre");
                        rol =  arr.getInt("rol");
                        emp =  arr.getInt("id_empresa");
                        func =  arr.getInt("funciones");
                        proy =  arr.getInt("proyecto");
                        punt =  arr.getInt("puntos");
                        rol_n = arr.getString("rol_nombre");
                        foto = arr.getString("imagen");
                        User.logged(true, this, token);
                        User.set("id_user", id, this);
                        User.set("id_empresa", emp, this);
                        User.set("id_rol", rol, this);
                        User.set("email", correo, this);
                        User.set("nombre", nombre, this);
                        User.set("rol_name", rol_n, this);
                        User.set("foto", foto, this);
                        User.set("funciones", func, this);
                        User.set("proyectos", proy, this);
                        User.set("puntos", punt, this);
                        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                        dispo = true;
                        HashMap<String, Object> params = new HashMap<>();
                        params.put("token_firebase", refreshedToken);
                        params.put("token", token);
                        WebBridge.send("registradevice=1&key=admin", params, "Registrando", this, this);

                    }else {
                        if (json.getBoolean("success")) {
                            logeado= true;
                            HashMap<String, Object> params = new HashMap<>();
                            params.put("email", email.getText().toString());
                            params.put("password", pass.getText().toString());
                            WebBridge.send("login=1&key=admin", params, "Enviando", this, this);
                        } else {
                            new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(json.getString("message")).setNeutralButton(R.string.btn_close, null).show();
                        }
                    }
                }
                else
                {
                    if (json.getBoolean("success")) {
                        JSONArray data2 = json.getJSONArray("data");
                        rolesArray.clear();
                        id_roles.clear();
                        for (int i = 0; i < data2.length(); i++) {
                            rolesArray.add(data2.getJSONObject(i).getString("rol"));
                            id_roles.add(data2.getJSONObject(i).getInt("id"));
                        }
                        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(
                                this, R.layout.spinner, rolesArray);
                        adapter2.setDropDownViewResource(R.layout.spinner_drop);
                        rol.setAdapter(adapter2);
                        //String t = data.getString("texto");
                        //Log.e("data->", t);
                        //txtTest.setText(t);
                        Log.e("texto-->", "se puso el texto");
                        rol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {

                                Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                                rol_id = id_roles.get(myPosition);
                                String z = id_roles.get(myPosition).toString();
                                Log.e("estado--->", z);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }
                        });

                    }else {
                        new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(json.getString("error_message")).setNeutralButton(R.string.btn_close, null).show();
                    }
                }
            }
            else
            {
                if (json.getBoolean("success")) {

                    JSONArray data = json.getJSONArray("data");
                    for (int i = 0; i < data.length(); i++) {
                        empresasArray.add(data.getJSONObject(i).getString("nombre"));
                        id_empresas.add(data.getJSONObject(i).getInt("id"));
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                            this, R.layout.spinner, empresasArray);
                    adapter.setDropDownViewResource(R.layout.spinner_drop);
                    empresa.setAdapter(adapter);
                    //String t = data.getString("texto");
                    //Log.e("data->", t);
                    //txtTest.setText(t);
                    cargado = true;
                    Log.e("texto-->", "se puso el texto");
                    empresa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {

                            Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            empresa_id = id_empresas.get(myPosition);
                            loadRoles(empresa_id);

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error-->", e.toString());
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {

    }
}
