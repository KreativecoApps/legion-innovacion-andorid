package com.kreativeco.legionkrea;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LogIn extends Activity implements WebBridge.WebBridgeListener{

    EditText email, pass;
    Boolean registrado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_in);
        email = (EditText) findViewById(R.id.edt_user);
        pass = (EditText) findViewById(R.id.edt_pass);
        registrado = false;
    }

    public void loadRecover(View view){
        Intent loginIntent = new Intent(LogIn.this, RecoverPass.class);
        startActivity(loginIntent);
    }

    public void loadRegistro(View view){
        Intent loginIntent = new Intent(LogIn.this, Register.class);
        startActivity(loginIntent);
    }

    public void goHome(){
        Intent loginIntent = new Intent(LogIn.this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        finish();
    }

    public void send(View view){
        HashMap<String, Object> params = new HashMap<>();
        params.put("email", email.getText().toString());
        params.put("password", pass.getText().toString());
        WebBridge.send("login=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")) {
                if (registrado){
                    FirebaseAnalytics mFirebaseAnalytics;
                    mFirebaseAnalytics = FirebaseAnalytics.getInstance(LogIn.this);
                    Bundle bundle = new Bundle();
                    bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
                    mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
                    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);
                    goHome();
                }else {
                    String token;
                    int id;
                    String correo;
                    String nombre;
                    int rol, emp, func, proy, punt;
                    String rol_n;
                    String foto;
                    JSONObject arr = json.getJSONObject("data");
                    token = arr.getString("token");
                    id = arr.getInt("id_user");
                    correo = arr.getString("email");
                    nombre = arr.getString("nombre");
                    rol = arr.getInt("rol");
                    emp = arr.getInt("id_empresa");
                    func = arr.getInt("funciones");
                    proy = arr.getInt("proyecto");
                    punt = arr.getInt("puntos");
                    rol_n = arr.getString("rol_nombre");
                    foto = arr.getString("imagen");
                    User.logged(true, this, token);
                    User.set("id_user", id, this);
                    User.set("id_empresa", emp, this);
                    User.set("id_rol", rol, this);
                    User.set("email", correo, this);
                    User.set("nombre", nombre, this);
                    User.set("rol_name", rol_n, this);
                    User.set("foto", foto, this);
                    User.set("funciones", func, this);
                    User.set("proyectos", proy, this);
                    User.set("puntos", punt, this);
                    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                    registrado = true;
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("token_firebase", refreshedToken);
                    params.put("token", token);
                    WebBridge.send("registradevice=1&key=admin", params, "Registrando", this, this);
                }
            } else {
                new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(json.getString("message")+json.getString("error_message")).setNeutralButton(R.string.btn_close, null).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error-->", e.toString());
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(json.getString("error_message")).setNeutralButton(R.string.btn_close, null).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
