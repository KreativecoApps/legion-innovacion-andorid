package com.kreativeco.legionkrea;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;

public class Idear extends Activity {

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    private Button mem, log, sol;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.idear);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        mem = (Button) findViewById(R.id.btn_mem);
        log = (Button) findViewById(R.id.btn_log);
        sol = (Button) findViewById(R.id.btn_sol);
        mem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadInst(1);
            }
        });
        log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadInst(2);
            }
        });
        sol.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadInst(3);
            }
        });
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("Idear_Activity", bundle);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Idear",this.getClass().getSimpleName());
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }

    public void loadInst(int type){
        Intent loginIntent = new Intent(this, Instructions.class);
        loginIntent.putExtra("int_value", type);
        startActivity(loginIntent);
    }

    public void loadMood(View view){
        Intent loginIntent = new Intent(this, MoodBoard.class);
        startActivity(loginIntent);
    }

    public void loadNProyect(View view){
        Intent loginIntent = new Intent(this, NewProyect.class);
        startActivity(loginIntent);
    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }

    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }

    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }

    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }
}
