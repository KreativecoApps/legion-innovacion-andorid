package com.kreativeco.legionkrea;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class DetLibraryVideo extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{

    String url="";
    String us="";
    int id = 0;
    int scrip=0;
    private Button btnSkipVideo;


    private static String YOU_TUBE_API_KEY = "AIzaSyDtxM9LEA-msfzbgVkv9LT9yyKhlOZt9sM";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        btnSkipVideo=(Button)findViewById(R.id.btn_skip_vid);

        YouTubePlayerView playerView = (YouTubePlayerView) findViewById(R.id.video_player);
        Bundle bundle = getIntent().getExtras();
        id = bundle.getInt("id");
        url = bundle.getString("URL");

        Log.e("primeraU-->",url);


        int inicio = url.indexOf("=");
        //     int fin = url.indexOf("=", inicio + 1);


        us=url.substring(inicio + 1);
        us = us.substring(0,11);



        Log.e("url-->",us);

        playerView.initialize(YOU_TUBE_API_KEY, this);


    }

    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onPlaying() {
            if (scrip==1) {
                btnSkipVideo.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPaused() {

        }

        @Override
        public void onStopped() {

        }

        @Override
        public void onBuffering(boolean b) {

        }

        @Override
        public void onSeekTo(int i) {

        }
    };


    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {

        @Override
        public void onLoading() {

        }

        @Override
        public void onLoaded(String s) {

        }

        @Override
        public void onAdStarted() {

        }

        @Override
        public void onVideoStarted() {

        }

        @Override
        public void onVideoEnded() {
            if (scrip==1) {

                Log.e("fin-->", "se acabo el video");
            }else {
                finish();
            }

        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {

        }
    };




    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

        youTubePlayer.setPlaybackEventListener(playbackEventListener);
        youTubePlayer.setPlayerStateChangeListener(playerStateChangeListener);
        if(!b){
            Log.e("cargar->","video");
            Log.e("URL", us);
            youTubePlayer.cueVideo(us);



        }


    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

        Log.e("fallo","fallo");
    }


    public void clickBack(View view) {

        finish();
    }


    public void endVideo(View view){

        Log.e("entro--->","termino video");
        finish();
    }
}