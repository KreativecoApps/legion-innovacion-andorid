package com.kreativeco.legionkrea.customsview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by omar on 8/4/16.
 */
public class TextViewMinima extends TextView {
    public TextViewMinima(Context context) {
        super(context);
        init();
    }

    public TextViewMinima(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewMinima(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public TextViewMinima(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init(){

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "minima_expanded.ttf");
        setTypeface(tf, 1);

    }
}
