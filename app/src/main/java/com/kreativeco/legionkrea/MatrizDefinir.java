package com.kreativeco.legionkrea;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MatrizDefinir extends Activity implements WebBridge.WebBridgeListener{

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    private JSONArray data, matrices;
    private Spinner cy;
    private int proyect, matriz;
    private TextView title, title2;
    private EditText body;
    private LinearLayout card1,card2;
    List<Integer> id_proyectos = new ArrayList<Integer>();
    List<String> spinnerArray =  new ArrayList<String>();
    Boolean cargado, enviado,validado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.matriz_definir);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        cy= (Spinner) findViewById(R.id.spinner1);
        title = (TextView) findViewById(R.id.edt_title);
        title2 = (TextView) findViewById(R.id.edt_title2);
        body = (EditText) findViewById(R.id.edt_body);
        card1= (LinearLayout) findViewById(R.id.lny_carda);
        card2= (LinearLayout) findViewById(R.id.lny_cards);
        cargado=false; enviado = false; validado = false;
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("MatrizDefinir_Activity", bundle);
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        WebBridge.send("listadoproyectosuser=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Matriz Definir",this.getClass().getSimpleName());
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }

    public void loadDefinir(View view){
        Intent loginIntent = new Intent(this, Definir.class);
        startActivity(loginIntent);
    }

    public void loadEvaluar(View view){
        Intent loginIntent = new Intent(this, MatrizEvaluar.class);
        loginIntent.putExtra("matriz",matriz);
        loginIntent.putExtra("proyect",proyect);
        startActivity(loginIntent);
    }

    public void loadNProyect(View view){
        Intent loginIntent = new Intent(this, NewProyect.class);
        startActivity(loginIntent);
    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }

    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }

    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }

    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    public void setMatriz(View view){
        Log.e("TAG: ", view.getTag().toString());
        try {
            int u = view.getId();
            matriz = Integer.parseInt(view.getTag().toString());
            if (matriz!=0) {
                card1.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
                card2.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
                view.setBackgroundResource(R.drawable.definir_matriz_opc_color);
                for (int i = 0; i < card1.getChildCount(); i++) {
                    View v = card1.getChildAt(i);
                    if (v instanceof TextView) {
                        if (u==card1.getId()){((TextView) v).setTextColor(Color.WHITE);}
                        else{((TextView) v).setTextColor(Color.BLACK);}
                    }

                }
                for (int i = 0; i < card2.getChildCount(); i++) {
                    View v = card2.getChildAt(i);
                    if (v instanceof TextView) {
                        if (u==card2.getId()){((TextView) v).setTextColor(Color.WHITE);}
                        else{((TextView) v).setTextColor(Color.BLACK);}
                    }
                }
                if (u==card1.getId()){title2.setText(matrices.getJSONObject(0).getString("nombre"));}
                else{title2.setText(matrices.getJSONObject(1).getString("nombre"));}
                HashMap<String, Object> params = new HashMap<>();
                params.put("token", User.getToken(this));
                params.put("id_matriz",matriz);
                params.put("id_proyecto", proyect);
                WebBridge.send("listadoproyectosmatrices=1&key=admin", params, "Enviando", this, this);
            }
        } catch(NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void send(View view) {

        ArrayList<String> errors = new ArrayList<String>();
        if (proyect==0) errors.add(getString(R.string.edttxt_error_proyect));
        if (matriz==0) errors.add(getString(R.string.edttxt_error_matriz));
        if (body.getText().length() < 1) errors.add(getString(R.string.edttxt_error_body));
        if (errors.size() != 0) {
            String msg = "";
            for (String s : errors) {
                msg += "- " + s + "\n";
            }
            new android.app.AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(msg.trim()).setNeutralButton(R.string.btn_close, null).show();
            return;
        }
        enviado = true;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id_proyecto",proyect);
        params.put("id_matriz",matriz);
        params.put("nota",body.getText());
        Log.e("---->",params.toString());
        WebBridge.send("guardarproyectomatriz=1&key=admin", params, "Enviando", this, this);


    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")) {
                if (enviado) {
                    new android.app.AlertDialog.Builder(MatrizDefinir.this)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle("Aviso")
                            .setMessage(json.getString("message"))
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    loadEvaluar(body);
                                }

                            })
                            .show();
                } else if (validado){
                    JSONArray dito = json.getJSONArray("data");
                    if (data.length()>0){
                        body.setText(dito.getJSONObject(0).getString("notas"));

                    }
                }else if (cargado) {
                    matrices = json.getJSONArray("data");
                    matriz = matrices.getJSONObject(0).getInt("id");
                    card1.setTag(matrices.getJSONObject(0).getInt("id"));
                    card2.setTag(matrices.getJSONObject(1).getInt("id"));
                    validado = true;
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("token", User.getToken(this));
                    params.put("id_matriz",matriz);
                    params.put("id_proyecto", proyect);
                    WebBridge.send("listadoproyectosmatrices=1&key=admin", params, "Enviando", this, this);
                } else {
                    data = json.getJSONArray("data");
                    for (int i = 0; i < data.length(); i++) {
                        spinnerArray.add(data.getJSONObject(i).getString("nombre"));
                        id_proyectos.add(data.getJSONObject(i).getInt("id"));
                    }
                    spinnerArray.add("Nuevo Proyecto");
                    id_proyectos.add(0);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                            this, R.layout.spinner, spinnerArray);
                    adapter.setDropDownViewResource(R.layout.spinner_drop);
                    cy.setAdapter(adapter);
                    Log.e("texto-->", "se puso el texto");
                    cy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {

                            Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            proyect = id_proyectos.get(myPosition);
                            if (myPosition != data.length()) {
                                try {
                                    title.setText(data.getJSONObject(myPosition).getString("nombre"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                title.setText("");
                                proyect = 0;
                                new android.app.AlertDialog.Builder(MatrizDefinir.this)
                                        .setIcon(android.R.drawable.ic_dialog_info)
                                        .setTitle("Aviso")
                                        .setMessage("Para añadir un personaje a un nuevo proyecto te mandaremos primero a crear el proyecto, estas seguro")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                loadNProyect(title);
                                                finish();
                                            }

                                        })
                                        .setNegativeButton("NO, registrar en otro proyecto", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                cy.setSelection(0);
                                            }

                                        })
                                        .show();

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });
                    cargado = true;
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("token", User.getToken(this));
                    WebBridge.send("listadomatrices=1&key=admin", params, "Enviando", this, this);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                if (User.logged(this)) {User.clear(this);}
                //LoginManager.getInstance().logOut();
                Intent intent = new Intent(this, LogIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
