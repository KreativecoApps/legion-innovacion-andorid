package com.kreativeco.legionkrea;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;
import com.kreativeco.legionkrea.utils.paint.PaintView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Personaje extends Activity implements WebBridge.WebBridgeListener{

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    PaintView pView;
    ImageView imgSign, imgAvatar,rlImage;
    ImageView btnMujer, btnHombre;
    LinearLayout lySign;
    RelativeLayout viewDraw;
    ImageView undoDraw, gomas;
    TextView tama;
    Button sendSign;
    String col;
    int typ, size;
    RelativeLayout person;
    private Button btnOk;
    private File imgSendSign;
    boolean imageSign = false;
    boolean goma = false;
    boolean cargado = false;
    boolean enviado = false;
    private JSONArray data, signos;
    private Spinner cy, spSigno;
    private int proyect=0, signo=0, emocion=0;
    List<Integer> id_proyectos = new ArrayList<Integer>();
    List<String> spinnerArray =  new ArrayList<String>();
    List<Integer> id_signos = new ArrayList<Integer>();
    List<String> signosArray =  new ArrayList<String>();
    private TextView title, desc;
    private EditText edtNombre, edtEdad, edtHobies, edtPlanes, edtSueños, edtJob, edtIng, edtEg;
    private ImageView l1, l2, l3, l4, l5;

    private static final int START_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE_RESULT = 0;
    private static int RESULT_LOAD_IMG = 1;

    private Dialog alert;
    private String strFileLocation = "";
    private File fileSendPhoto = null;

    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personaje);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.e("Este no fue","rayos");
            if (this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                Log.e("prueba", "pruebas");
                requestPermissions(new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE},MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            } else {
                Log.d("Home", "Already granted access");
            }
        }
        person = (RelativeLayout) findViewById(R.id.personaje);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        cy= (Spinner) findViewById(R.id.spinner1);
        title = (TextView) findViewById(R.id.edt_title);
        desc = (EditText) findViewById(R.id.edt_desc);
        edtNombre = (EditText) findViewById(R.id.edt_nombre);
        edtEdad = (EditText) findViewById(R.id.edt_edad);
        spSigno = (Spinner) findViewById(R.id.edt_signo);
        edtHobies = (EditText) findViewById(R.id.edt_hobbies);
        edtPlanes = (EditText) findViewById(R.id.edt_planes);
        edtSueños = (EditText) findViewById(R.id.edt_sueños);
        edtJob = (EditText) findViewById(R.id.edt_trabajo);
        edtIng = (EditText) findViewById(R.id.edt_money_ob);
        edtEg = (EditText) findViewById(R.id.edt_money_es);
        l1 = (ImageView) findViewById(R.id.liker1);
        l2 = (ImageView) findViewById(R.id.liker2);
        l3 = (ImageView) findViewById(R.id.liker3);
        l4 = (ImageView) findViewById(R.id.liker4);
        l5 = (ImageView) findViewById(R.id.liker5);
        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEmote(view,1);
            }
        });
        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEmote(view,2);
            }
        });
        l3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEmote(view,3);
            }
        });
        l4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEmote(view,4);
            }
        });
        l5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEmote(view,5);
            }
        });
        viewDraw = (RelativeLayout) findViewById(R.id.rl_draw);
        lySign = (LinearLayout) findViewById(R.id.ly_sign);
        undoDraw = (ImageView) findViewById(R.id.img_undo);
        btnOk = (Button) findViewById(R.id.ok_sign);
        rlImage = (ImageView) findViewById(R.id.rl_image);
        btnHombre = (ImageView) findViewById(R.id.icon_hombre);
        btnMujer = (ImageView) findViewById(R.id.icon_mujer);
        undoDraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickUndoDraw(v);
            }
        });
        imgSign = (ImageView) findViewById(R.id.img_sign);
        imgAvatar = (ImageView) findViewById(R.id.img_avatar);
        gomas = (ImageView)findViewById(R.id.goma);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                okSing();
            }
        });
        tama = (TextView) findViewById(R.id.tama);
        pView = new PaintView(this);
        typ = 0;
        size= 12;
        col = "#000000";
        viewDraw.addView(pView);
        imgSign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lySign.setVisibility(View.VISIBLE);
                person.setVisibility(View.GONE);
                pView.clear();
            }
        });
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("Personajes_Activity", bundle);
        WebBridge.send("listadosignos=1&key=admin", "Enviando", this, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Personajes",this.getClass().getSimpleName());
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }

    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    public void loadJourney(View view){
        Intent loginIntent = new Intent(this, Empatizar.class);
        startActivity(loginIntent);
        finish();
    }

    public void loadEmpatizar(View view){
        Intent loginIntent = new Intent(this, Empatizar.class);
        startActivity(loginIntent);
        finish();
    }

    public void loadNProyect(View view){
        Intent loginIntent = new Intent(this, NewProyect.class);
        startActivity(loginIntent);
    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }

    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }

    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }

    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("Home", "Permission Granted");
                    //initializeView(v);
                } else {
                    Log.d("Home", "Permission Failed");
                    Toast.makeText(getBaseContext(), "Para grabar audio necesitas otorgar los permisos para grabar.", Toast.LENGTH_SHORT).show();
                }
            }
            // Add additional cases for other permissions you may have asked for
        }
    }

    private void config() {
       /* btTypeDraw.setText( getOption("pencil_type", R.array.data_draw_types) );
        btColorDraw.setText("Borrar");*/
    }

    public void setCol(View view){
        col = view.getTag().toString();
        update(typ,col,size);
    }

    public void plusType(View view){
        if (!goma) {
            if (size < 30) {
                size++;
                tama.setText(size + "");
            }
            update(typ, col, size);
        }
    }

    public void minType(View view){
        if (!goma){
        if (size>0){
            size--;
            tama.setText(size+"");
        }
        update(typ,col,size);
        }
    }

    public void gomaOnOff(View view){
        if (goma){
            typ = 0;
            size = size -10;
            gomas.setColorFilter(Color.RED);
            update(typ,col,size);
            goma=false;
        }else{
            typ = 2;
            size = size+10;
            gomas.clearColorFilter();
            update(typ,col,size);
            goma=true;
        }
    }

    private void update(int typeSelector, String colors, int sizes) {
        pView.setEnabled(true);
        int color = Color.parseColor(colors);//getOption("pencil_color", R.array.data_draw_colors_values));
        pView.setColor(color);
        pView.setType(typeSelector);//getInt("pencil_type"));
        pView.setSize(sizes);

    }

    public void changeMan(View view){
        Glide.with(this).load(R.drawable.empatizar_personaje_hombre).into(imgAvatar);
        Glide.with(this).load(R.drawable.empatizar_personaje_hombre).into(rlImage);
        Glide.with(this).load(R.drawable.empatizar_personaje_hombre_on).into(btnHombre);
        Glide.with(this).load(R.drawable.empatizar_personaje_mujer_off).into(btnMujer);
    }
    public void changeWoman(View view){
        Glide.with(this).load(R.drawable.empatizar_personaje_mujer).into(imgAvatar);
        Glide.with(this).load(R.drawable.empatizar_personaje_mujer).into(rlImage);
        Glide.with(this).load(R.drawable.empatizar_personaje_hombre_off).into(btnHombre);
        Glide.with(this).load(R.drawable.empatizar_personaje_mujer_on).into(btnMujer);
    }

    public void clickUndoDraw(View v) {
        pView.undo();
    }

    /*public void clickTypeDraw(View v) {
        update(0);
    }

    public void clickColorDraw(View v) {
        update(2);
    }*/

    public void setEmote(View view, int emote){
        if (emocion==0) {
            l1.setVisibility(View.GONE);
            l2.setVisibility(View.GONE);
            l3.setVisibility(View.GONE);
            l4.setVisibility(View.GONE);
            l5.setVisibility(View.GONE);
            view.setVisibility(View.VISIBLE);
            emocion = emote;
        }else{
            l1.setVisibility(View.VISIBLE);
            l2.setVisibility(View.VISIBLE);
            l3.setVisibility(View.VISIBLE);
            l4.setVisibility(View.VISIBLE);
            l5.setVisibility(View.VISIBLE);
            emocion = 0;
        }
    }

    public void okSing() {

        View u = findViewById(R.id.rl_draw);
        u.setDrawingCacheEnabled(true);

        Bitmap b = Bitmap.createBitmap(u.getDrawingCache());
        u.setDrawingCacheEnabled(false);

        //Save bitmap
        File dir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        if (!dir.exists())
            dir.mkdirs();

        String fileName;
        Calendar cal = Calendar.getInstance();
        int millisecond = cal.get(Calendar.MILLISECOND);
        int second = cal.get(Calendar.SECOND);
        int minute = cal.get(Calendar.MINUTE);
        int hourofday = cal.get(Calendar.HOUR_OF_DAY);
        fileName = "image_" + hourofday + "" + minute + "" + second + ""
                + millisecond + ".png";

        File myPath = new File(dir, fileName);
        imgSendSign=myPath;
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(myPath);
            b.compress(Bitmap.CompressFormat.PNG, 50, fos);
            fos.flush();
            fos.close();
            ContentResolver resolver = this.getContentResolver();
            MediaStore.Images.Media.insertImage(resolver, b, "Screen", "screen");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e("error--->",e.toString());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("error--->",e.toString());
        }

        Glide.with(this).load(myPath).into(imgAvatar);
        Glide.with(this).load(myPath).into(rlImage);
        lySign.setVisibility(View.GONE);
        person.setVisibility(View.VISIBLE);
        imageSign=true;
    }

    public void clickCamera(View v) {

        alert = new Dialog(this);
        alert.setTitle(getResources().getString(R.string.txt_select_option));
        alert.setContentView(getLayoutInflater().inflate(R.layout.dialog_alert_photo, null));
        alert.findViewById(R.id.bt_select_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.cancel();
                takePhoto();
            }
        });

        alert.findViewById(R.id.bt_select_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.cancel();
                loadImagefromGallery(v);
            }
        });

        alert.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.cancel();
            }
        });

        alert.show();

    }

    @TargetApi(Build.VERSION_CODES.M)
    private void takePhoto() {

        if (ContextCompat.checkSelfPermission(Personaje.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            callCameraApp();
        } else {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(Personaje.this, getString(R.string.txt_request_permissions), Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_EXTERNAL_STORAGE_RESULT);
        }
    }

    private void callCameraApp() {

        Intent callCameraIntent = new Intent();
        if (Build.VERSION.SDK_INT >= 23)
            callCameraIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE_SECURE);
        else callCameraIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri photoURI = null;

        try {
            File photoFile = createImageFile();
            photoURI = FileProvider.getUriForFile(Personaje.this,
                    getString(R.string.file_provider_authority),
                    photoFile);
        } catch (Exception e) {
            Log.e("EXCEPTION", e.toString());
        }

        callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        startActivityForResult(callCameraIntent, START_CAMERA);

    }

    File createImageFile() throws IOException {

        String strDate = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String nameImage = "image_" + strDate + "_";

        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(nameImage, ".jpg", storageDir);
        strFileLocation = image.getAbsolutePath();

        return image;
    }

    public void loadImagefromGallery(View view) {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                // Get the Image from data
                Log.e("galeria","entro");
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                strFileLocation = cursor.getString(columnIndex);

                cursor.close();

                // Set the Image in ImageView after decoding the String
                savePhoto(strFileLocation);

            } else if (requestCode == START_CAMERA && resultCode == RESULT_OK) {
                Log.e("camara","entro");
                savePhoto(strFileLocation);

            }else {
                Toast.makeText(this, "No haz seleccionado una imagen", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {

            Toast.makeText(this, "Ocurrió algún error", Toast.LENGTH_LONG).show();

        }

    }

    public void savePhoto(String image) {
        fileSendPhoto = new File(image);
        /*HashMap<String, Object> params = new HashMap<>();
        params.put("foto", fileSendPhoto);
        params.put("token", User.getToken(this));*/
        Log.e("foto foto", fileSendPhoto.toString());
        Glide.with(this).load(strFileLocation).into(imgAvatar);
        Glide.with(this).load(strFileLocation).into(rlImage);
        Glide.with(this).load(R.drawable.empatizar_personaje_hombre_off).into(btnHombre);
        Glide.with(this).load(R.drawable.empatizar_personaje_mujer_off).into(btnMujer);
        new AlertDialog.Builder(this).setTitle("Exito").setMessage("Foto subida con exito").setNeutralButton(R.string.btn_close, null).show();
    }

    public void send(View view) {

        ArrayList<String> errors = new ArrayList<String>();
        if (proyect==0) errors.add(getString(R.string.edttxt_error_proyect));
        if (desc.getText().length() < 1) errors.add(getString(R.string.edttxt_error_desc));
        if (edtNombre.getText().length() < 1) errors.add(getString(R.string.edttxt_error_nombre));
        if (!imageSign) errors.add(getString(R.string.edttxt_error_image));
        if (edtEdad.getText().length() < 1) errors.add(getString(R.string.edttxt_error_edad));
        if (signo==0) errors.add(getString(R.string.edttxt_error_signo));
        if (edtHobies.getText().length() < 1) errors.add(getString(R.string.edttxt_error_hobies));
        if (edtPlanes.getText().length() < 1) errors.add(getString(R.string.edttxt_error_planes));
        if (edtSueños.getText().length() < 1) errors.add(getString(R.string.edttxt_error_sueños));
        if (emocion==0) errors.add(getString(R.string.edttxt_error_emocion));
        if (edtJob.getText().length() < 1) errors.add(getString(R.string.edttxt_error_job));
        if (edtIng.getText().length() < 1) errors.add(getString(R.string.edttxt_error_ingreso));
        if (edtEg.getText().length() < 1) errors.add(getString(R.string.edttxt_error_egreso));

        if (errors.size() != 0) {
            String msg = "";
            for (String s : errors) {
                msg += "- " + s + "\n";
            }
            new android.app.AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(msg.trim()).setNeutralButton(R.string.btn_close, null).show();
            return;
        }
        enviado = true;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id_proyecto",proyect);
        params.put("descripcion",desc.getText());
        params.put("nombre", edtNombre.getText());
        params.put("imagen",imgSendSign);
        params.put("edad", edtEdad.getText());
        params.put("signo", signo);
        params.put("hobbies", edtHobies.getText());
        params.put("planes", edtPlanes.getText());
        params.put("suenos", edtSueños.getText());
        params.put("emociones", emocion);
        params.put("trabajo", edtJob.getText());
        params.put("dinero_ganado", edtIng.getText());
        params.put("dinero_gastado", edtEg.getText());
        Log.e("---->",params.toString());
        WebBridge.send("guardarpersonaje=1&key=admin", params, "Enviando", this, this);


    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")){
                if (enviado){
                    new android.app.AlertDialog.Builder(this)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle("Aviso")
                            .setMessage(json.getString("message"))
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    loadEmpatizar(title);
                                }

                            })
                            .show();
                } else if(cargado){
                    data = json.getJSONArray("data");
                    for (int i = 0; i < data.length(); i++) {
                        spinnerArray.add(data.getJSONObject(i).getString("nombre"));
                        id_proyectos.add(data.getJSONObject(i).getInt("id"));
                    }
                    spinnerArray.add("Nuevo Proyecto");
                    id_proyectos.add(0);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                            this, R.layout.spinner, spinnerArray);
                    adapter.setDropDownViewResource(R.layout.spinner_drop);
                    cy.setAdapter(adapter);
                    Log.e("texto-->", "se puso el texto");
                    cy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {

                            Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            proyect = id_proyectos.get(myPosition);
                            if (myPosition != data.length()) {
                                try {
                                    title.setText(data.getJSONObject(myPosition).getString("nombre"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                title.setText("");
                                proyect=0;
                                new android.app.AlertDialog.Builder(Personaje.this)
                                        .setIcon(android.R.drawable.ic_dialog_info)
                                        .setTitle("Aviso")
                                        .setMessage("Para añadir un personaje a un nuevo proyecto te mandaremos primero a crear el proyecto, estas seguro")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                new android.app.AlertDialog.Builder(Personaje.this)
                                                        .setIcon(android.R.drawable.ic_dialog_info)
                                                        .setTitle("Aviso")
                                                        .setMessage("Si sales de esta pantalla perderas todo el avance que hayas hecho, no quieres registrar tu personaje en otro proyecto?")
                                                        .setPositiveButton("OK, registrar en otro proyecto", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                cy.setSelection(0);
                                                            }

                                                        })
                                                        .setNegativeButton("NO, crear un nuevo proyecto", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {
                                                                loadNProyect(l1);
                                                                finish();
                                                            }

                                                        })
                                                        .show();
                                            }

                                        })
                                        .setNegativeButton("NO, registrar en otro proyecto", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                cy.setSelection(0);
                                            }

                                        })
                                        .show();

                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });
                }else {
                    signos = json.getJSONArray("data");
                    for (int i = 0; i < signos.length(); i++) {
                        signosArray.add(signos.getJSONObject(i).getString("nombre"));
                        id_signos.add(signos.getJSONObject(i).getInt("id"));
                    }
                    ArrayAdapter<String> adapters = new ArrayAdapter<String>(
                            this, R.layout.spinners, signosArray);
                    adapters.setDropDownViewResource(R.layout.spinner_drops);
                    spSigno.setAdapter(adapters);
                    Log.e("texto-->", "se puso el texto");
                    spSigno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {
                            Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            signo = id_signos.get(myPosition);
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });
                    cargado=true;
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("token", User.getToken(this));
                    WebBridge.send("listadoproyectosuser=1&key=admin", params, "Enviando", this, this);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                if (User.logged(this)) {User.clear(this);}
                //LoginManager.getInstance().logOut();
                Intent intent = new Intent(this, LogIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
