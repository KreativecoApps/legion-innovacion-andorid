package com.kreativeco.legionkrea;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.adapters.TaskCompAdapter;
import com.kreativeco.legionkrea.adapters.TaskPendAdapter;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Testing extends Activity implements WebBridge.WebBridgeListener{

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    private RecyclerView recyclerList, recyclerList2;
    private Spinner cy;
    private List<Integer> id_proyectos = new ArrayList<Integer>();
    private List<String> spinnerArray =  new ArrayList<String>();
    private  int proyect;
    private Boolean cargado, enviando;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testing);
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("Testing_Activity", bundle);
        //todo();
    }
    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Testing",this.getClass().getSimpleName());
        todo();
    }

    public void todo(){
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        cy= (Spinner) findViewById(R.id.spinner1);
        recyclerList = (RecyclerView) findViewById(R.id.recycler_list);
        recyclerList.setHasFixedSize(false);
        RecyclerView.LayoutManager rvLayoutManager = new LinearLayoutManager(this);
        recyclerList.setLayoutManager(rvLayoutManager);
        recyclerList2 = (RecyclerView) findViewById(R.id.recycler_list2);
        recyclerList2.setHasFixedSize(false);
        RecyclerView.LayoutManager rvLayoutManager2 = new LinearLayoutManager(this);
        recyclerList2.setLayoutManager(rvLayoutManager2);
        cargado = false; enviando = false;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        WebBridge.send("listadoproyectosuser=1&key=admin", params, "Enviando", this, this);
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }

    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    public void loadTestear(View view){
        Intent loginIntent = new Intent(this, Feedback.class);
        startActivity(loginIntent);
    }

    public void loadNProyect(View view){
        Intent loginIntent = new Intent(this, NewProyect.class);
        startActivity(loginIntent);
    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }

    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }

    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }

    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    public void loadNTask(View view){
        Intent loginIntent = new Intent(this, NuevaTarea.class);
        loginIntent.putExtra("proyect",proyect);
        startActivity(loginIntent);
    }

    public void completeTask(int task_id){
        enviando = true;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id",task_id);
        params.put("estatus",2);
        WebBridge.send("actualizarestatustareasretrosol=1&key=admin", params, "Enviando", this, this);
    }

    public void getTasks(){
        cargado = true;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id_proyecto",proyect);
        WebBridge.send("listadoevaluacionretrosol=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")){
                if (enviando){
                    new AlertDialog.Builder(this)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle("Aviso")
                            .setMessage(json.getJSONObject("data").getString("msg"))
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    enviando=false;
                                    getTasks();
                                }

                            })
                            .show();
                }else if(cargado){
                    JSONObject data = json.getJSONObject("data");
                    JSONArray realizadas = data.getJSONArray("realizadas");
                    JSONArray pendientes = data.getJSONArray("pendientes");
                    RecyclerView.Adapter rvAdapter = new TaskCompAdapter(realizadas,this);
                    recyclerList.setAdapter(rvAdapter);
                    RecyclerView.Adapter rvAdapter2 = new TaskPendAdapter(pendientes,this);
                    recyclerList2.setAdapter(rvAdapter2);
                }else{
                    JSONArray data2 = json.getJSONArray("data");
                    /*spinnerArray.add("Nuevo Proyecto");
                    id_proyectos.add(0);*/
                    for (int i = 0; i < data2.length(); i++) {
                        spinnerArray.add(data2.getJSONObject(i).getString("nombre"));
                        id_proyectos.add(data2.getJSONObject(i).getInt("id"));
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                            this, R.layout.spinner_rr, spinnerArray);
                    adapter.setDropDownViewResource(R.layout.spinner_drop_rr);
                    cy.setAdapter(adapter);
                    Log.e("texto-->", "se puso el texto");
                    cy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {

                            Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            proyect = id_proyectos.get(myPosition);
                            getTasks();
                            /*if (myPosition != 0) {
                                try {
                                    title.setText(data.getJSONObject(myPosition - 1).getString("nombre"));
                                    body.setText(data.getJSONObject(myPosition - 1).getString("descripcion"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                title.setText("");
                                body.setText("");
                            }*/
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {

    }
}
