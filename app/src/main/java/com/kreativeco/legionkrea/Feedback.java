package com.kreativeco.legionkrea;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Feedback extends Activity implements WebBridge.WebBridgeListener{

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    private Spinner cy;
    List<Integer> id_proyectos = new ArrayList<Integer>();
    List<String> spinnerArray =  new ArrayList<String>();
    JSONArray data;
    private int proyect, feedback, carta;
    private Boolean cargado, validado;
    private LinearLayout card1, card2, card3, card4;
    private EditText body;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        cy= (Spinner) findViewById(R.id.spinner1);
        body = (EditText) findViewById(R.id.edt_body);
        card1 = (LinearLayout) findViewById(R.id.lny_cards);
        card2 = (LinearLayout) findViewById(R.id.lny_cards2);
        card3 = (LinearLayout) findViewById(R.id.lny_cards3);
        card4 = (LinearLayout) findViewById(R.id.lny_cards4);
        validado = false; cargado = false;
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("Feedback_Activity", bundle);
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        WebBridge.send("listadoproyectosuser=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Feedback",this.getClass().getSimpleName());
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }

    public void loadTesting(View view){
        Intent loginIntent = new Intent(this, Testing.class);
        startActivity(loginIntent);
    }

    public void loadNProyect(View view){
        Intent loginIntent = new Intent(this, NewProyect.class);
        startActivity(loginIntent);
    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }

    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }

    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }

    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    public void send (View view){
        ArrayList<String> errors = new ArrayList<String>();
        if (body.length() < 1) errors.add(getString(R.string.edit_text_error_body));
        if (errors.size() != 0) {
            String msg = "";
            for (String s : errors) {
                msg += "- " + s + "\n";
            }
            new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(msg.trim()).setNeutralButton(R.string.btn_close, null).show();
            return;
        }
        validado = true;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id_tipo",carta);
        params.put("id_proyecto", proyect);
        params.put("notas",body.getText().toString());
        WebBridge.send("guardarretrosol=1&key=admin", params, "Enviando", this, this);
    }

    public void setFeedback(View view) {
        Log.e("TAG: ", view.getTag().toString());
        try {
            int u = view.getId();
            feedback = Integer.parseInt(view.getTag().toString());
            if (feedback != 0) {
                card1.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
                card2.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
                card3.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
                card4.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
                view.setBackgroundResource(R.drawable.definir_matriz_opc_color);
                for (int i = 0; i < card1.getChildCount(); i++) {
                    View v = card1.getChildAt(i);
                    if (v instanceof TextView) {
                        if (u == card1.getId()) {
                            ((TextView) v).setTextColor(Color.WHITE);
                        } else {
                            ((TextView) v).setTextColor(Color.BLACK);
                        }
                    }
                    if (v instanceof ImageView) {
                        if (u == card1.getId()) {
                            ((ImageView) v).setColorFilter(Color.WHITE);
                        } else {
                            ((ImageView) v).setColorFilter(Color.BLACK);
                        }
                    }
                }
                for (int i = 0; i < card2.getChildCount(); i++) {
                    View v = card2.getChildAt(i);
                    if (v instanceof TextView) {
                        if (u == card2.getId()) {
                            ((TextView) v).setTextColor(Color.WHITE);
                        } else {
                            ((TextView) v).setTextColor(Color.BLACK);
                        }
                    }
                    if (v instanceof ImageView) {
                        if (u == card2.getId()) {
                            ((ImageView) v).setColorFilter(Color.WHITE);
                        } else {
                            ((ImageView) v).setColorFilter(Color.BLACK);
                        }
                    }
                }
                for (int i = 0; i < card3.getChildCount(); i++) {
                    View v = card3.getChildAt(i);
                    if (v instanceof TextView) {
                        if (u == card3.getId()) {
                            ((TextView) v).setTextColor(Color.WHITE);
                        } else {
                            ((TextView) v).setTextColor(Color.BLACK);
                        }
                    }
                    if (v instanceof ImageView) {
                        if (u == card3.getId()) {
                            ((ImageView) v).setColorFilter(Color.WHITE);
                        } else {
                            ((ImageView) v).setColorFilter(Color.BLACK);
                        }
                    }
                }
                for (int i = 0; i < card4.getChildCount(); i++) {
                    View v = card4.getChildAt(i);
                    if (v instanceof TextView) {
                        if (u == card4.getId()) {
                            ((TextView) v).setTextColor(Color.WHITE);
                        } else {
                            ((TextView) v).setTextColor(Color.BLACK);
                        }
                    }
                    if (v instanceof ImageView) {
                        if (u == card4.getId()) {
                            ((ImageView) v).setColorFilter(Color.WHITE);
                        } else {
                            ((ImageView) v).setColorFilter(Color.BLACK);
                        }
                    }
                }
                if (u == card1.getId()) {
                    carta =1;
                } else if (u == card2.getId()) {
                    carta =2;
                } else if (u == card3.getId()) {
                    carta =3;
                } else {
                    carta =4;
                }
            }
        } catch (NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try{
            if (json.getBoolean("success")) {
                if (validado){
                    new AlertDialog.Builder(this)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle("Aviso")
                            .setMessage(json.getString("msg"))
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    validado=false;
                                }

                            })
                            .show();
                }else if(cargado){
                    data = json.getJSONArray("data");
                    feedback = data.getJSONObject(0).getInt("id");
                    card1.setTag(data.getJSONObject(0).getInt("id"));
                    card2.setTag(data.getJSONObject(1).getInt("id"));
                    card3.setTag(data.getJSONObject(2).getInt("id"));
                    card4.setTag(data.getJSONObject(3).getInt("id"));
                }else {
                    data = json.getJSONArray("data");
                    /*spinnerArray.add("Nuevo Proyecto");
                    id_proyectos.add(0);*/
                    for (int i = 0; i < data.length(); i++) {
                        spinnerArray.add(data.getJSONObject(i).getString("nombre"));
                        id_proyectos.add(data.getJSONObject(i).getInt("id"));
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                            this, R.layout.spinner_rr, spinnerArray);
                    adapter.setDropDownViewResource(R.layout.spinner_drop_rr);
                    cy.setAdapter(adapter);
                    Log.e("texto-->", "se puso el texto");
                    cy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {

                            Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            proyect = id_proyectos.get(myPosition);
                            /*if (myPosition != 0) {
                                try {
                                    title.setText(data.getJSONObject(myPosition - 1).getString("nombre"));
                                    body.setText(data.getJSONObject(myPosition - 1).getString("descripcion"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                title.setText("");
                                body.setText("");
                            }*/
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });
                    cargado = true;
                    WebBridge.send("listadotiposretrosol=1&key=admin", "Enviando", this, this);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                if (User.logged(this)) {User.clear(this);}
                //LoginManager.getInstance().logOut();
                Intent intent = new Intent(this, LogIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
