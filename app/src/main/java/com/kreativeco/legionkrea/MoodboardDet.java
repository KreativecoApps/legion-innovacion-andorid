package com.kreativeco.legionkrea;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;

import org.json.JSONException;
import org.json.JSONObject;

public class MoodboardDet extends Activity {

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    JSONObject moodboard;
    private TextView title, body, pdf;
    private ImageView camara, imagen;
    int mood;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.moodboard_det);
        title = (TextView) findViewById(R.id.txt_title);
        body = (TextView) findViewById(R.id.txt_body);
        camara = (ImageView) findViewById(R.id.img_image);
        imagen = (ImageView) findViewById(R.id.img_image2);
        pdf = (TextView) findViewById(R.id.txt_pdf);
        try {
            moodboard = new JSONObject(getIntent().getStringExtra("mood"));
            String name= moodboard.getString("nombre");
            String desc= moodboard.getString("descripcion");
            String cam= moodboard.getString("camara");
            String img= moodboard.getString("imagen");
            final String adj= moodboard.getString("adjunto");
            title.setText(name);
            body.setText(desc);
            mood = moodboard.getInt("id");
            if (cam.equals("")){camara.setVisibility(View.GONE);}else{Glide.with(this).load(cam).into(camara);}
            if (img.equals("")){imagen.setVisibility(View.GONE);}else{Glide.with(this).load(img).into(imagen);}
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("MoodboardDet_Activity", bundle);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Detalle Moodboard",this.getClass().getSimpleName());
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }



    public void loadIdear(View view){
        Intent loginIntent = new Intent(this, Idear.class);
        startActivity(loginIntent);
    }

    public void loadNProyect(View view){
        Intent loginIntent = new Intent(this, NewProyect.class);
        startActivity(loginIntent);
    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }

    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }

    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }

    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }
}
