package com.kreativeco.legionkrea;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class Profile extends Activity implements WebBridge.WebBridgeListener{

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;

    private TextView rol, puntos, proyect, function;
    private EditText nombre;
    private ImageView foto, bot;
    private Boolean editando;

    private static final int START_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE_RESULT = 0;
    private static int RESULT_LOAD_IMG = 1;
    private static int RESULT_LOAD_FILE = 2;

    private Dialog alert;
    private String strFotoLocation = "";
    private File fileSendPhoto = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        nombre =(EditText) findViewById(R.id.edt_nombre);
        rol = (TextView)findViewById(R.id.txt_rol);
        puntos = (TextView) findViewById(R.id.txt_puntos);
        proyect = (TextView) findViewById(R.id.txt_proyect);
        function = (TextView) findViewById(R.id.txt_function);
        foto = (ImageView)findViewById(R.id.txt_title);
        bot = (ImageView) findViewById(R.id.img_foto);
        blockCampos();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("Profile_Activity", bundle);
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        WebBridge.send("miperfil=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Perfil",this.getClass().getSimpleName());
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }
    public void loadProyects(View view){
        if (editando) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("No se han guardado los cambios")
                    .setMessage("¿Deseas guardar los cambios?")
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateProfile();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent loginIntent = new Intent(Profile.this, Proyects.class);
                            startActivity(loginIntent);
                        }

                    })
                    .show();
        }
        else{
            Intent loginIntent = new Intent(this, Proyects.class);
            startActivity(loginIntent);
        }

    }
    public void loadRanking(View view){
        if (editando) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("No se han guardado los cambios")
                    .setMessage("¿Deseas guardar los cambios?")
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateProfile();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent loginIntent = new Intent(Profile.this, Ranking.class);
                            startActivity(loginIntent);
                        }

                    })
                    .show();
        }
        else{
            Intent loginIntent = new Intent(this, Ranking.class);
            startActivity(loginIntent);
        }
    }
    public void loadLibrary(View view){
        if (editando) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("No se han guardado los cambios")
                    .setMessage("¿Deseas guardar los cambios?")
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateProfile();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent loginIntent = new Intent(Profile.this, Biblioteca.class);
                            startActivity(loginIntent);
                        }

                    })
                    .show();
        }
        else{
            Intent loginIntent = new Intent(this, Biblioteca.class);
            startActivity(loginIntent);
        }
    }

    public void loadNots(View view){
        if (editando) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("No se han guardado los cambios")
                    .setMessage("¿Deseas guardar los cambios?")
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateProfile();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent loginIntent = new Intent(Profile.this, NotifTrue.class);
                            startActivity(loginIntent);
                        }

                    })
                    .show();
        }
        else{
            Intent loginIntent = new Intent(this, NotifTrue.class);
            startActivity(loginIntent);
        }

    }

    public void loadAjustes(View view){
        if (editando) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("No se han guardado los cambios")
                    .setMessage("¿Deseas guardar los cambios?")
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateProfile();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent loginIntent = new Intent(Profile.this, Ajustes.class);
                            startActivity(loginIntent);
                        }

                    })
                    .show();
        }
        else{
            Intent loginIntent = new Intent(this, Ajustes.class);
            startActivity(loginIntent);
        }
    }

    public void goHome(View view){
        if (editando) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("No se han guardado los cambios")
                    .setMessage("¿Deseas guardar los cambios?")
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateProfile();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent loginIntent = new Intent(Profile.this, Home.class);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(loginIntent);
                        }

                    })
                    .show();
        }
        else{
            Intent loginIntent = new Intent(this, Ajustes.class);
            startActivity(loginIntent);
        }

    }

    @Override
    public void onBackPressed() {
        if (editando) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("No se han guardado los cambios")
                    .setMessage("¿Deseas guardar los cambios?")
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            updateProfile();
                        }

                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }

                    })
                    .show();
        }
        else{
            finish();
        }
    }

    public void blockCampos(){
        editando=false;
        nombre.setEnabled(false);
        nombre.setFocusable(false);
        nombre.setFocusableInTouchMode(false);
        bot.setVisibility(View.GONE);
    }

    public void editProfile(View view){
        if (editando){
            updateProfile();
        }
        else {
            nombre.setEnabled(true);
            nombre.setFocusable(true);
            nombre.setFocusableInTouchMode(true);
            editando = true;
            bot.setVisibility(View.VISIBLE);
        }
    }

    public void updateProfile(){
        ArrayList<String> errors = new ArrayList<>();
        if (nombre.getText().length() < 1) errors.add(getString(R.string.edit_text_error_name));
        //if (strFotoLocation.length() < 1) errors.add(getString(R.string.edit_text_error_foto));
        if (errors.size() != 0) {
            String msg = "";
            for (String s : errors) {
                msg += "- " + s + "\n";
            }
            new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(msg.trim()).setNeutralButton(R.string.btn_close, null).show();
            return;
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("nombre", nombre.getText().toString());
        if (!strFotoLocation.equals("")){params.put("imagen", fileSendPhoto);}
        WebBridge.send("editarmiperfil=1&key=admin", params, "Actualizando", this, this);
    }

    public void clickCamera(View v) {

        alert = new Dialog(this);
        alert.setTitle(getResources().getString(R.string.txt_select_option));
        alert.setContentView(getLayoutInflater().inflate(R.layout.dialog_alert_photo, null));
        alert.findViewById(R.id.bt_select_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.cancel();
                takePhoto();
            }
        });

        alert.findViewById(R.id.bt_select_photo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.cancel();
                loadImagefromGallery(v);
            }
        });

        alert.findViewById(R.id.bt_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.cancel();
            }
        });

        alert.show();

    }


    @TargetApi(Build.VERSION_CODES.M)
    private void takePhoto() {

        if (ContextCompat.checkSelfPermission(Profile.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            callCameraApp();
        } else {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(Profile.this, getString(R.string.txt_request_permissions), Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_EXTERNAL_STORAGE_RESULT);
        }
    }

    private void callCameraApp() {

        Intent callCameraIntent = new Intent();
        if (Build.VERSION.SDK_INT >= 23)
            callCameraIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE_SECURE);
        else callCameraIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri photoURI = null;

        try {
            File photoFile = createImageFile();
            photoURI = FileProvider.getUriForFile(Profile.this,
                    getString(R.string.file_provider_authority),
                    photoFile);
        } catch (Exception e) {
            Log.e("EXCEPTION", e.toString());
        }

        callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        startActivityForResult(callCameraIntent, START_CAMERA);

    }

    File createImageFile() throws IOException {

        String strDate = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String nameImage = "image_" + strDate + "_";

        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(nameImage, ".jpg", storageDir);
        strFotoLocation = image.getAbsolutePath();

        return image;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void loadImagefromGallery(View view) {
        // Create intent to Open Image applications like Gallery, Google Photos
        if (ContextCompat.checkSelfPermission(Profile.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            // Start the Intent
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        } else {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(Profile.this, getString(R.string.txt_request_permissions), Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_EXTERNAL_STORAGE_RESULT);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                // Get the Image from data
                Log.e("galeria","entro");
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                assert cursor != null;
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                strFotoLocation = cursor.getString(columnIndex);

                cursor.close();

                // Set the Image in ImageView after decoding the String
                savePhoto(strFotoLocation);

            }else if (requestCode == START_CAMERA && resultCode == RESULT_OK) {
                Log.e("camara","entro");
                savePhoto(strFotoLocation);

            }else {
                Toast.makeText(this, "No haz seleccionado una imagen", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {

            if (strFotoLocation.equals(""))Toast.makeText(this, "Ocurrió algún error", Toast.LENGTH_LONG).show();
        }

    }

    public void savePhoto(String image) {
        fileSendPhoto = new File(image);
        /*HashMap<String, Object> params = new HashMap<>();
        params.put("foto", fileSendPhoto);
        params.put("token", User.getToken(this));*/
        Log.e("foto foto",fileSendPhoto.toString());
        Glide.with(this).load(strFotoLocation).into(foto);
        new android.support.v7.app.AlertDialog.Builder(this).setTitle("Exito").setMessage("Foto subida con exito").setNeutralButton(R.string.btn_close, null).show();
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")) {
                if (editando){
                    blockCampos();
                    new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(json.getString("message")).setNeutralButton(R.string.btn_close, null).show();
                } else {
                    JSONArray data = json.getJSONArray("data");
                    JSONObject prof = data.getJSONObject(0);
                    nombre.setText(prof.getString("nombre"));
                    if (!prof.getString("imagen").equals("http://kreativeco.com/legion/usuarios/")) {
                        Glide.with(Profile.this).load(prof.getString("imagen")).into(foto);
                    }
                    rol.setText("Rol: " + prof.getString("rol"));
                    puntos.setText(prof.getString("puntos") + " puntos");
                    proyect.setText(prof.getString("proyectos") + "");
                    function.setText(prof.getString("funciones") + "");
                }
            }else {
                new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(json.getString("error_message")).setNeutralButton(R.string.btn_close, null).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                if (User.logged(this)) {User.clear(this);}
                //LoginManager.getInstance().logOut();
                Intent intent = new Intent(this, LogIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
