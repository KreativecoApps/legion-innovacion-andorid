package com.kreativeco.legionkrea;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.adapters.LibAdapter;
import com.kreativeco.legionkrea.adapters.LibDetAdapter;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Biblioteca extends Activity implements WebBridge.WebBridgeListener{

    private RecyclerView recyclerList, recyclerCates;
    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    private JSONArray data,data2;
    private int longi;
    private Boolean cargado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.biblioteca);
        recyclerList = (RecyclerView) findViewById(R.id.recycler_list);
        recyclerList.setHasFixedSize(false);
        RecyclerView.LayoutManager rvLayoutManager = new LinearLayoutManager(getBaseContext());
        recyclerList.setLayoutManager(rvLayoutManager);

        recyclerCates = (RecyclerView) findViewById(R.id.recycler_cates);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });

        cargado = false;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        WebBridge.send("categoriasbiblioteca=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Biblioteca",this.getClass().getSimpleName());
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }

    public void loadNProyect(View view){
        Intent loginIntent = new Intent(this, NewProyect.class);
        startActivity(loginIntent);
    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }

    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }
    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }
    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    public void DetLibrary(int id){
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id_categoria",id);
        WebBridge.send("biblioteca=1&key=admin", params, "Enviando", this, this);
    }

    public void DetLibraryLoadPDF(int id, String url, String type){
        Intent loginIntent = new Intent(this, DetLibrary.class);
        loginIntent.putExtra("id",id);
        loginIntent.putExtra("URL",url);
        loginIntent.putExtra("type",type);
        startActivity(loginIntent);
    }

    public void DetLibraryLoadVideo(int id, String url){
        Intent loginIntent = new Intent(this, DetLibraryVideo.class);
        loginIntent.putExtra("id",id);
        loginIntent.putExtra("URL",url);
        startActivity(loginIntent);
    }

    public void InfoLibrary(View view){
        Intent loginIntent = new Intent(this, LibraryInfo.class);
        startActivity(loginIntent);
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")){
                if (cargado) {
                    data2 = json.getJSONArray("data");
                    RecyclerView.Adapter rvAdapter = new LibDetAdapter(data2,this);
                    recyclerList.setAdapter(rvAdapter);
                }else{
                    data = json.getJSONArray("data");
                    recyclerCates.setHasFixedSize(false);
                    GridLayoutManager cateLayoutManager = new GridLayoutManager(getBaseContext(), 3);
                    cateLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            //define span size for this position
                            //for example, if you have 2 column per row, you can implement something like that:
                            try {
                                JSONObject actual = data.getJSONObject(position);
                                String muestra = actual.getString("nombre");
                                longi = muestra.length();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (longi > 24) {
                                return 3; //item will take 2 column (full row size)
                            } else if (longi > 12) {
                                return 2;
                            } else {
                                return 1; //you will have 2 rolumn per row
                            }
                        }
                    });
                    recyclerCates.setLayoutManager(cateLayoutManager);
                    RecyclerView.Adapter cateAdapter = new LibAdapter(data, this);
                    recyclerCates.setAdapter(cateAdapter);
                    cargado=true;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("exception", e.toString());

            Log.e(this.toString(), e.toString());
            new AlertDialog.Builder(this).setTitle(R.string.txt_error)
                    .setMessage(R.string.error_exception)
                    .setNeutralButton(R.string.btn_close, null).show();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                if (User.logged(this)) {User.clear(this);}
                //LoginManager.getInstance().logOut();
                Intent intent = new Intent(this, LogIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
