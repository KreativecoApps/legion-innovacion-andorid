package com.kreativeco.legionkrea;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MatrizEvaluar extends Activity implements WebBridge.WebBridgeListener{

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    private int proyect, matriz, submatriz, carta, stars;
    private TextView title;
    private EditText body;
    private JSONArray data;
    private LinearLayout card1, card2, card3, card4;
    private Boolean validado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.matriz_evaluar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        matriz= getIntent().getExtras().getInt("matriz",0);
        proyect= getIntent().getExtras().getInt("proyect",0);
        title = (TextView) findViewById(R.id.edt_title);
        carta = 1;
        card1 = (LinearLayout) findViewById(R.id.lny_cards);
        card2 = (LinearLayout) findViewById(R.id.lny_cards2);
        card3 = (LinearLayout) findViewById(R.id.lny_cards3);
        card4 = (LinearLayout) findViewById(R.id.lny_cards4);
        body = (EditText) findViewById(R.id.edt_body);
        validado = false;
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("MatrizEvaluar_Activity", bundle);
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id_matriz",matriz);
        WebBridge.send("listadosubmatrices=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Matriz Evaluar",this.getClass().getSimpleName());
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }

    public void loadDefinir(View view){
        Intent loginIntent = new Intent(this, Definir.class);
        startActivity(loginIntent);
    }

    public void loadTerminar(View view){
        ArrayList<String> errors = new ArrayList<String>();
        if (proyect==0) errors.add(getString(R.string.edttxt_error_proyect));
        if (matriz==0) errors.add(getString(R.string.edttxt_error_matriz));
        if (submatriz==0) errors.add(getString(R.string.edttxt_error_submatriz));
        if (body.getText().length() < 1) errors.add(getString(R.string.edttxt_error_body));
        if (errors.size() != 0) {
            String msg = "";
            for (String s : errors) {
                msg += "- " + s + "\n";
            }
            new android.app.AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(msg.trim()).setNeutralButton(R.string.btn_close, null).show();
            return;
        }
        Intent loginIntent = new Intent(this, MatrizTerminar.class);
        loginIntent.putExtra("proyect", proyect);
        loginIntent.putExtra("matriz", matriz);
        loginIntent.putExtra("submatriz", submatriz);
        loginIntent.putExtra("carta", carta);
        loginIntent.putExtra("stars", stars);
        loginIntent.putExtra("nota", body.getText().toString());
        loginIntent.putExtra("title", title.getText().toString());
        startActivity(loginIntent);
    }

    public void loadNProyect(View view){
        Intent loginIntent = new Intent(this, NewProyect.class);
        startActivity(loginIntent);
    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }

    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }

    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }

    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    /*public void send(View view) {

        ArrayList<String> errors = new ArrayList<String>();
        if (proyect==0) errors.add(getString(R.string.edttxt_error_proyect));
        if (matriz==0) errors.add(getString(R.string.edttxt_error_proyect));
        if (body.getText().length() < 1) errors.add(getString(R.string.edttxt_error_desc));
        if (errors.size() != 0) {
            String msg = "";
            for (String s : errors) {
                msg += "- " + s + "\n";
            }
            new android.app.AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(msg.trim()).setNeutralButton(R.string.btn_close, null).show();
            return;
        }
        enviado = true;

        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id_proyecto",proyect);
        params.put("id_matriz",matriz);
        params.put("id_submatriz", submatriz);
        params.put("nota",body.getText());
        Log.e("---->",params.toString());
        WebBridge.send("guardarproyectomatriz=1&key=admin", params, "Enviando", this, this);


    }*/

    public void setMatriz(View view) {
        Log.e("TAG: ", view.getTag().toString());
        try {
            int u = view.getId();
            submatriz = Integer.parseInt(view.getTag().toString());
            if (submatriz != 0) {
                card1.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
                card2.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
                card3.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
                card4.setBackgroundResource(R.drawable.definir_matriz_opc_blanco);
                view.setBackgroundResource(R.drawable.definir_matriz_opc_color);
                for (int i = 0; i < card1.getChildCount(); i++) {
                    View v = card1.getChildAt(i);
                    if (v instanceof TextView) {
                        if (u == card1.getId()) {
                            ((TextView) v).setTextColor(Color.WHITE);
                        } else {
                            ((TextView) v).setTextColor(Color.BLACK);
                        }
                    }

                }
                for (int i = 0; i < card2.getChildCount(); i++) {
                    View v = card2.getChildAt(i);
                    if (v instanceof TextView) {
                        if (u == card2.getId()) {
                            ((TextView) v).setTextColor(Color.WHITE);
                        } else {
                            ((TextView) v).setTextColor(Color.BLACK);
                        }
                    }
                }
                for (int i = 0; i < card3.getChildCount(); i++) {
                    View v = card3.getChildAt(i);
                    if (v instanceof TextView) {
                        if (u == card3.getId()) {
                            ((TextView) v).setTextColor(Color.WHITE);
                        } else {
                            ((TextView) v).setTextColor(Color.BLACK);
                        }
                    }
                }
                for (int i = 0; i < card4.getChildCount(); i++) {
                    View v = card4.getChildAt(i);
                    if (v instanceof TextView) {
                        if (u == card4.getId()) {
                            ((TextView) v).setTextColor(Color.WHITE);
                        } else {
                            ((TextView) v).setTextColor(Color.BLACK);
                        }
                    }
                }
                if (u == card1.getId()) {
                    title.setText(data.getJSONObject(0).getString("nombre"));
                    carta =1;
                } else if (u == card2.getId()) {
                    title.setText(data.getJSONObject(1).getString("nombre"));
                    carta =2;
                } else if (u == card3.getId()) {
                    title.setText(data.getJSONObject(2).getString("nombre"));
                    carta =3;
                } else {
                    title.setText(data.getJSONObject(3).getString("nombre"));
                    carta =4;
                }
                HashMap<String, Object> params = new HashMap<>();
                params.put("token", User.getToken(this));
                params.put("id_matriz",matriz);
                params.put("id_proyecto", proyect);
                params.put("id_submatriz",submatriz);
                WebBridge.send("listadosubmatricesproyectos=1&key=admin", params, "Enviando", this, this);
            }
        } catch (NumberFormatException nfe) {
            System.out.println("Could not parse " + nfe);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")){
                if (validado){
                    JSONArray dito = json.getJSONArray("data");
                    if (data.length()>0){
                        body.setText(dito.getJSONObject(0).getString("nota"));
                        stars = dito.getJSONObject(0).getInt("estrellas");
                    }
                }else {
                    data = json.getJSONArray("data");
                    submatriz = data.getJSONObject(0).getInt("id");
                    card1.setTag(data.getJSONObject(0).getInt("id"));
                    card2.setTag(data.getJSONObject(1).getInt("id"));
                    card3.setTag(data.getJSONObject(2).getInt("id"));
                    card4.setTag(data.getJSONObject(3).getInt("id"));
                    validado = true;
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("token", User.getToken(this));
                    params.put("id_matriz",matriz);
                    params.put("id_proyecto", proyect);
                    params.put("id_submatriz",submatriz);
                    WebBridge.send("listadosubmatricesproyectos=1&key=admin", params, "Enviando", this, this);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                if (User.logged(this)) {User.clear(this);}
                //LoginManager.getInstance().logOut();
                Intent intent = new Intent(this, LogIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
