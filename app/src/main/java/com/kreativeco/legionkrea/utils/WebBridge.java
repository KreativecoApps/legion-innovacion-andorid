package com.kreativeco.legionkrea.utils;

/**
 * Created by Sagamagus on 21/06/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.kreativeco.legionkrea.R;
import com.kreativeco.legionkrea.dialogs.ProgressDialog;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class WebBridge {

    private ProgressDialog progress;
    private WebBridgeListener callback;
    private AsyncHttpClient client;
    private String url;

    //static public String endpoint ="http://secure.kreativeco.com/pymes/api/";
    //https://alertamujeres.org.mx/app/ws.php?
    static public String endpoint = "http://kreativeco.com/legion/ws.php?";

    static public ArrayList<WebBridge> instances = new ArrayList<WebBridge>();

    static public String url(String url, Activity a) {
        if (url.indexOf("http://") == 0) return url;

        int mode = User.mode(a);
        String u = endpoint + url;

        return u;
    }


    static private void addVersion(Map<String, Object> params, Activity activity) {
        String app_version = "N/A";
        try {
            app_version = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
        } catch (Exception e) {
        }

        params.put("app_version", app_version);
        params.put("app_os", "android");
    }


    /**
     * Send without Token
     **/


    static public WebBridge send(String url, HashMap<String, Object> params, String message, Activity activity, WebBridgeListener callback) {
        WebBridge wb = WebBridge.getInstance(activity, message, callback);
        if (wb != null) {
            wb.send(WebBridge.url(url, activity), params);
        }
        return wb;
    }


    static public WebBridge send(String url, HashMap<String, Object> params, Activity activity, WebBridgeListener callback) {
        WebBridge wb = WebBridge.getInstance(activity, null, callback);
        if (wb != null) {
            wb.send(WebBridge.url(url, activity), params);
        }
        return wb;
    }


    static public WebBridge send(String url, String message, Activity activity, WebBridgeListener callback) {
        WebBridge wb = WebBridge.getInstance(activity, message, callback);
        if (wb != null) {
            wb.send(WebBridge.url(url, activity), null);
        }
        return wb;
    }


    static public WebBridge send(String url, HashMap<String, Object> params, Activity activity) {
        WebBridge wb = WebBridge.getInstance(activity, null, null);
        if (wb != null) {
            wb.send(WebBridge.url(url, activity), params);
        }
        return wb;
    }


    static public WebBridge send(String url, Activity activity, WebBridgeListener callback) {
        WebBridge wb = WebBridge.getInstance(activity, null, callback);
        if (wb != null) {
            wb.send(WebBridge.url(url, activity), null);
        }
        return wb;
    }

    /**
     * send with token
     **/


    static public WebBridge send(String url, String token, HashMap<String, Object> params, String message, Activity activity, WebBridgeListener callback) {
        WebBridge wb = WebBridge.getInstance(activity, token, message, callback);
        if (wb != null) {
            wb.send(WebBridge.url(url, activity), params);
        }
        return wb;
    }


    static public WebBridge send(String url, String token, HashMap<String, Object> params, Activity activity, WebBridgeListener callback) {
        WebBridge wb = WebBridge.getInstance(activity, token, null, callback);
        if (wb != null) {
            wb.send(WebBridge.url(url, activity), params);
        }
        return wb;
    }


    static public WebBridge send(String url, String token, String message, Activity activity, WebBridgeListener callback) {
        WebBridge wb = WebBridge.getInstance(activity, token, message, callback);
        if (wb != null) {
            wb.send(WebBridge.url(url, activity), null);
        }
        return wb;
    }


    static public WebBridge send(String url, String token, HashMap<String, Object> params, Activity activity) {
        WebBridge wb = WebBridge.getInstance(activity, token, null, null);
        if (wb != null) {
            wb.send(WebBridge.url(url, activity), params);
        }
        return wb;
    }


    static WebBridge getInstance(Activity activity, String message, WebBridgeListener callback) {

        if (WebBridge.haveNetworkConnection(activity) == false) {
            Toast.makeText(activity, activity.getResources().getString(R.string.error_connectivity), Toast.LENGTH_LONG).show();
            return null;
        }

        WebBridge wb = new WebBridge();
        wb.callback = callback;
        //wb.client = new AsyncHttpClient(true, 80, 443);
        wb.client = new AsyncHttpClient();
        wb.client.setTimeout(60000);
        PersistentCookieStore cookie = new PersistentCookieStore(activity);
        wb.client.setCookieStore(cookie);

        if (message != null)
            wb.progress = ProgressDialog.show(activity, message, true, false, null);

        WebBridge.instances.add(wb);
        return wb;

    }

    static WebBridge getInstance(Activity activity, String token, String message, WebBridgeListener callback) {

        if (!WebBridge.haveNetworkConnection(activity)) {
            Toast.makeText(activity, activity.getResources().getString(R.string.error_connectivity), Toast.LENGTH_LONG).show();
            return null;
        }

        WebBridge wb = new WebBridge();
        wb.callback = callback;
        wb.client = new AsyncHttpClient(true, 80, 443);
        //wb.client = new AsyncHttpClient();
        wb.client.addHeader("Authorization", "Bearer " + token);
        wb.client.setTimeout(60000);
        PersistentCookieStore cookie = new PersistentCookieStore(activity);
        wb.client.setCookieStore(cookie);

        if (message != null)
            wb.progress = ProgressDialog.show(activity, message, true, false, null);
        WebBridge.instances.add(wb);

        return wb;
    }


    protected void send(String url, HashMap<String, Object> p) {

        this.url = url;

        AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String response = null;
                try {
                    response = new String(responseBody, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                success(response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("Status 2", "" + statusCode);
                Log.e("Throw", error.toString());
                Log.e("Error", response);
                //Log.e("Header", headers.toString());
                failure(response);

            }
        };

        /*AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                String response = null;
                try {
                    response = new String(responseBody, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                success(response);
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                String response = "";
                try {
                    response = new String(responseBody, "UTF-8");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.e("Status 2", "" + statusCode);
                Log.e("Throw", error.toString());
                Log.e("Error", response);
                //Log.e("Header", headers.toString());
                failure(response);
            }

        };*/

        if (p != null) {
            RequestParams params = new RequestParams();

            for (Map.Entry<String, Object> entry : p.entrySet()) {
                String k = entry.getKey();
                Object v = entry.getValue();
                if (v instanceof File) {
                    File f = (File) v;
                    try {
                        params.put(k, f, "image/jpeg");
                        //params.put(k,f);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                } else {
                    params.put(k, v);
                }
            }

            Log.e("PARAMS:", p.toString());
            client.post(url, params, handler);
        } else {
            client.get(url, handler);
        }

    }


    public void failure(String response) {
        if (progress != null) {
            progress.dismiss();
            progress = null;
        }
        if (callback != null) {
            callback.onWebBridgeFailure(url, response);
        }
    }


    public void success(String response) {

        Log.e("RESPONSE", response.toString());

        if (progress != null) {
            progress.dismiss();
            progress = null;
        }

        if (callback != null) {
            JSONObject json = null;
            JSONArray jsonArray = null;
            try {
                json = new JSONObject(response);
                callback.onWebBridgeSuccess(url, json);
            } catch (Exception e) {
                try {
                    jsonArray = new JSONArray(response);
                    callback.onWebBridgeSuccess(url, jsonArray);

                } catch (JSONException j) {

                }
            }

        }

        WebBridge.instances.remove(this);

    }


    static public boolean haveNetworkConnection(Activity a) {
        try {


            boolean haveConnectedWifi = false;
            boolean haveConnectedMobile = false;

            ConnectivityManager cm = (ConnectivityManager) a.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected()) {
                        haveConnectedWifi = true;
                    }
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected()) {
                        haveConnectedMobile = true;
                    }
            }

            return haveConnectedWifi || haveConnectedMobile;
        } catch (Exception e) {
            Log.e("error-->", e.toString());
        }
        return false;
    }

    static public boolean isNetworkAvailable(Activity a) {
        ConnectivityManager connectivityManager = (ConnectivityManager) a.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public interface WebBridgeListener {
        void onWebBridgeSuccess(String url, JSONObject json);
        void onWebBridgeSuccess(String url, JSONArray json);
        void onWebBridgeFailure(String url, String response);
    }

}
