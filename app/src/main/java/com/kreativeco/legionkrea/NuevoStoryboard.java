package com.kreativeco.legionkrea;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.adapters.StoryListAdapter;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class NuevoStoryboard extends Activity implements WebBridge.WebBridgeListener{

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    private JSONArray data2;
    private EditText title, body, edtURL;
    private LinearLayout caja;
    private ImageView btnFoto, btnImagen, btnAudio, btnVideo, btnFile;
    private ImageView color1, color2, color3, color4, emoticon1, emoticon2, emoticon3, emoticon4, emoticon5;
    private String type = "new";
    private int proyect=0, color, emoticon, caj, stor;
    private Spinner cy;
    List<Integer> id_proyectos = new ArrayList<Integer>();
    List<String> spinnerArray =  new ArrayList<String>();
    private String strVideoLocation = "";

    private static final int START_CAMERA = 0;
    private static final int REQUEST_EXTERNAL_STORAGE_RESULT = 0;
    private static int RESULT_LOAD_IMG = 1;
    private static final int FILE_SELECT_CODE = 2;

    private String strFotoLocation = "";
    private File fileSendPhoto = null;
    private String strImageLocation = "";
    private File fileSendImage = null;
    private String strFileLocation = "";
    private File fileSendFile = null;

    private MediaRecorder recorder = null;
    private String strAudioLocation = "";
    private File fileSendAudio = null;
    public Boolean grabando, cargado, enviando;
    private RecyclerView recyclerList;

    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nuevo_storyboard);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.e("Este no fue","rayos");
            if (this.checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                Log.e("prueba", "pruebas");
                requestPermissions(new String[]{ Manifest.permission.RECORD_AUDIO},MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
            } else {
                Log.d("Home", "Already granted access");
            }
        }
        grabando=false;
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        color = 0; emoticon=0;
        caja = (LinearLayout) findViewById(R.id.ui_url);
        edtURL = (EditText) findViewById(R.id.edt_url);
        title = (EditText) findViewById(R.id.edt_title);
        body = (EditText) findViewById(R.id.edt_body);
        btnFoto = (ImageView) findViewById(R.id.foto);
        btnImagen = (ImageView) findViewById(R.id.imagen);
        btnVideo = (ImageView) findViewById(R.id.video);
        btnAudio = (ImageView) findViewById(R.id.audio);
        btnFile = (ImageView) findViewById(R.id.file);
        cy= (Spinner) findViewById(R.id.spinner1);
        btnFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePhoto(view);
            }
        });
        btnImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadImagefromGallery(view);
            }
        });
        btnVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getVideo();
            }
        });
        btnAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (grabando) {
                    Log.e("Stop Recording:", "");
                    btnAudio.setColorFilter(Color.DKGRAY);
                    grabando=false;
                    stopRecording();
                }else {
                    Log.e("Start Recording:", "");
                    btnAudio.setColorFilter(Color.RED);
                    grabando=true;
                    startRecording();
                }
            }
        });
        btnFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });
        color1 = (ImageView) findViewById(R.id.color1);
        color2 = (ImageView) findViewById(R.id.color2);
        color3 = (ImageView) findViewById(R.id.color3);
        color4 = (ImageView) findViewById(R.id.color4);
        emoticon1 = (ImageView) findViewById(R.id.emoticon1);
        emoticon2 = (ImageView) findViewById(R.id.emoticon2);
        emoticon3 = (ImageView) findViewById(R.id.emoticon3);
        emoticon4 = (ImageView) findViewById(R.id.emoticon4);
        emoticon5 = (ImageView) findViewById(R.id.emoticon5);
        color1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                color = 1;
                Log.e("color:",color+"");
                color1.setColorFilter(Color.LTGRAY);
                color2.clearColorFilter();
                color3.clearColorFilter();
                color4.clearColorFilter();
            }
        });
        color2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                color = 2;
                Log.e("color:",color+"");
                color1.clearColorFilter();
                color2.setColorFilter(Color.LTGRAY);
                color3.clearColorFilter();
                color4.clearColorFilter();
            }
        });
        color3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                color = 3;
                Log.e("color:",color+"");
                color1.clearColorFilter();
                color2.clearColorFilter();
                color3.setColorFilter(Color.LTGRAY);
                color4.clearColorFilter();
            }
        });
        color4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                color = 4;
                Log.e("color:",color+"");
                color1.clearColorFilter();
                color2.clearColorFilter();
                color3.clearColorFilter();
                color4.setColorFilter(Color.LTGRAY);
            }
        });
        emoticon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emoticon = 1;
                Log.e("emoticon:",emoticon+"");
                emoticon1.setBackgroundResource(R.drawable.likert_1_on);
                emoticon2.setBackgroundResource(R.drawable.likert_2_off);
                emoticon3.setBackgroundResource(R.drawable.likert_3_off);
                emoticon4.setBackgroundResource(R.drawable.likert_4_off);
                emoticon5.setBackgroundResource(R.drawable.likert_5_off);
            }
        });
        emoticon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emoticon = 2;
                Log.e("emoticon:",emoticon+"");
                emoticon1.setBackgroundResource(R.drawable.likert_1_off);
                emoticon2.setBackgroundResource(R.drawable.likert_2_on);
                emoticon3.setBackgroundResource(R.drawable.likert_3_off);
                emoticon4.setBackgroundResource(R.drawable.likert_4_off);
                emoticon5.setBackgroundResource(R.drawable.likert_5_off);
            }
        });
        emoticon3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emoticon = 3;
                Log.e("emoticon:",emoticon+"");
                emoticon1.setBackgroundResource(R.drawable.likert_1_off);
                emoticon2.setBackgroundResource(R.drawable.likert_2_off);
                emoticon3.setBackgroundResource(R.drawable.likert_3_on);
                emoticon4.setBackgroundResource(R.drawable.likert_4_off);
                emoticon5.setBackgroundResource(R.drawable.likert_5_off);
            }
        });
        emoticon4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emoticon = 4;
                Log.e("emoticon:",emoticon+"");
                emoticon1.setBackgroundResource(R.drawable.likert_1_off);
                emoticon2.setBackgroundResource(R.drawable.likert_2_off);
                emoticon3.setBackgroundResource(R.drawable.likert_3_off);
                emoticon4.setBackgroundResource(R.drawable.likert_4_on);
                emoticon5.setBackgroundResource(R.drawable.likert_5_off);
            }
        });
        emoticon5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emoticon = 5;
                Log.e("emoticon:",emoticon+"");
                emoticon1.setBackgroundResource(R.drawable.likert_1_off);
                emoticon2.setBackgroundResource(R.drawable.likert_2_off);
                emoticon3.setBackgroundResource(R.drawable.likert_3_off);
                emoticon4.setBackgroundResource(R.drawable.likert_4_off);
                emoticon5.setBackgroundResource(R.drawable.likert_5_on);
            }
        });
        recyclerList = (RecyclerView) findViewById(R.id.recycler_list);
        recyclerList.setHasFixedSize(false);
        RecyclerView.LayoutManager rvLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerList.setLayoutManager(rvLayoutManager);
        caj = getIntent().getIntExtra("objeto",0);
        stor = getIntent().getIntExtra("id",0);
        proyect = getIntent().getIntExtra("proyect",0);
        cargado = false; enviando = false;
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("NewStoryboard_Activity", bundle);
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        WebBridge.send("listadoproyectosuser=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"New Storyaboard",this.getClass().getSimpleName());
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }
    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    public void loadStDet(JSONObject story){
        try {
            if (story.has("id")) {
                type = "act";
                stor = story.getInt("id");
                title.setText(story.getString("nombre"));
                body.setText(story.getString("descripcion"));
                color = story.getInt("color");
                emoticon = story.getInt("emoticon");
            }else{
                type = "new";
                stor = 0;
                title.setText("");
                body.setText("");
                color = 0;
                emoticon = 0;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }
    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }
    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }
    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public  void getVideo(){
        caja.setVisibility(View.VISIBLE);
    }

    public void saveVideo(View view){
        if (!edtURL.getText().equals("")){
            strVideoLocation = edtURL.getText().toString();
            btnVideo.setColorFilter(Color.DKGRAY);
        }
        caja.setVisibility(View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("Home", "Permission Granted");
                    //initializeView(v);
                } else {
                    Log.d("Home", "Permission Failed");
                    Toast.makeText(getBaseContext(), "Para grabar audio necesitas otorgar los permisos para grabar.", Toast.LENGTH_SHORT).show();
                }
            }
            // Add additional cases for other permissions you may have asked for
        }
    }

    private String getFilename(){
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,"AudioRecorder");

        if(!file.exists()){
            file.mkdirs();
        }

        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".m4a");
    }

    private void startRecording(){
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        strAudioLocation =getFilename();
        recorder.setOutputFile(strAudioLocation);
        recorder.setOnErrorListener(errorListener);
        recorder.setOnInfoListener(infoListener);

        try {
            recorder.prepare();
            recorder.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
            Log.e("Error: " + what, extra+"");
        }
    };

    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
            Log.e("Warning: " + what,"" + extra);
        }
    };

    private void stopRecording(){
        if(null != recorder){
            recorder.stop();
            recorder.reset();
            recorder.release();
            recorder = null;
            fileSendAudio = new File(strAudioLocation);
            btnAudio.setColorFilter(Color.DKGRAY);
            new android.support.v7.app.AlertDialog.Builder(this).setTitle("Exito").setMessage("Audio grabado con exito").setNeutralButton(R.string.btn_close, null).show();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void takePhoto(View view) {

        if (ContextCompat.checkSelfPermission(NuevoStoryboard.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            callCameraApp();
        } else {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(NuevoStoryboard.this, getString(R.string.txt_request_permissions), Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_EXTERNAL_STORAGE_RESULT);
        }
    }

    private void callCameraApp() {

        Intent callCameraIntent = new Intent();
        if (Build.VERSION.SDK_INT >= 23)
            callCameraIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE_SECURE);
        else callCameraIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        Uri photoURI = null;

        try {
            File photoFile = createImageFile();
            photoURI = FileProvider.getUriForFile(NuevoStoryboard.this,
                    getString(R.string.file_provider_authority),
                    photoFile);
        } catch (Exception e) {
            Log.e("EXCEPTION", e.toString());
        }

        callCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        startActivityForResult(callCameraIntent, START_CAMERA);

    }

    File createImageFile() throws IOException {

        String strDate = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String nameImage = "image_" + strDate + "_";

        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(nameImage, ".jpg", storageDir);
        strFotoLocation = image.getAbsolutePath();

        return image;
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void loadImagefromGallery(View view) {
        // Create intent to Open Image applications like Gallery, Google Photos
        if (ContextCompat.checkSelfPermission(NuevoStoryboard.this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            // Start the Intent
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        } else {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(NuevoStoryboard.this, getString(R.string.txt_request_permissions), Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_EXTERNAL_STORAGE_RESULT);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                // Get the Image from data
                Log.e("galeria","entro");
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                assert cursor != null;
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                strFileLocation = cursor.getString(columnIndex);
                cursor.close();
                // Set the Image in ImageView after decoding the String
                //savePhoto(strFotoLocation);
                fileSendFile = new File(strFileLocation);
                btnImagen.setColorFilter(Color.DKGRAY);
                new android.support.v7.app.AlertDialog.Builder(this).setTitle("Exito").setMessage("Imagen subida con exito").setNeutralButton(R.string.btn_close, null).show();
            }else if (requestCode == START_CAMERA && resultCode == RESULT_OK) {
                Log.e("camara","entro");
                //savePhoto(strFotoLocation);
                fileSendPhoto = new File(strFotoLocation);
                btnFoto.setColorFilter(Color.DKGRAY);
                new android.support.v7.app.AlertDialog.Builder(this).setTitle("Exito").setMessage("Foto subida con exito").setNeutralButton(R.string.btn_close, null).show();
            }else if (requestCode == FILE_SELECT_CODE && resultCode == RESULT_OK) {
                // Get the Uri of the selected file
                Uri uri = data.getData();
                Log.e("uri:","File Uri: " + uri.toString());

                /*String[] projection = {MediaStore.Files.FileColumns.DATA};
                // Get the cursor
                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                // Move to first row
                assert cursor != null;
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(projection[0]);
                strImageLocation = cursor.getString(columnIndex);
                cursor.close();*/
                strImageLocation=getFilePath(this,uri);
                /*String filepath = Environment.getExternalStorageDirectory().getPath();
                //File file = new File(filepath,"AudioRecorder");
                String fileuri = uri.getPath();
                Log.e("uri:","path: "+ fileuri);
                Log.e("uri:","root path: "+ filepath);
                //strImageLocation = fileuri.replace("/document/3264-6630:",filepath+"/");*/
                Log.e("uri:","true path: "+ strImageLocation);
                //strImageLocation = uri.getPath();
                fileSendImage = new File(strImageLocation);
                btnFile.setColorFilter(Color.DKGRAY);
                new android.support.v7.app.AlertDialog.Builder(this).setTitle("Exito").setMessage("Archivo subido con exito").setNeutralButton(R.string.btn_close, null).show();
            }else {
                Toast.makeText(this, "No haz seleccionado una imagen", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.e("Excepcion:",e.toString());
            if (strFotoLocation.equals(""))Toast.makeText(this, "Ocurrió algún error", Toast.LENGTH_LONG).show();
        }

    }

    public static String getFilePath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    public void send(View view){
        ArrayList<String> errors = new ArrayList<String>();
        if (title.length() < 1) errors.add(getString(R.string.edit_text_error_titulo));
        if (body.length() < 1) errors.add(getString(R.string.edit_text_error_body));
        if (strFileLocation.equals("")&&strFotoLocation.equals(""))errors.add(getString(R.string.edttxt_error_imaqen));
        if (emoticon==0) errors.add(getString(R.string.error_emoticon));
        if (color==0) errors.add(getString(R.string.error_color));
        if (errors.size() != 0) {
            String msg = "";
            for (String s : errors) {
                msg += "- " + s + "\n";
            }
            new AlertDialog.Builder(this).setTitle(R.string.txt_error).setMessage(msg.trim()).setNeutralButton(R.string.btn_close, null).show();
            return;
        }
        enviando = true;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("nombre", title.getText().toString());
        params.put("descripcion", body.getText().toString());
        params.put("color",color);
        params.put("emoticon",emoticon);
        params.put("camara", fileSendPhoto);
        params.put("imagen", fileSendFile);
        params.put("audio", fileSendAudio);
        params.put("video", strVideoLocation);
        params.put("adjunto", fileSendImage);
        Log.e("ES este:", params.toString());
        if (type.equals("new")){
            params.put("id_proyecto", proyect);
            WebBridge.send("guardarstoryboard=1&key=admin", params, "Enviando", this, this);
        }else{
            params.put("id_storyboard", stor);
            WebBridge.send("actualizarstoryboard=1&key=admin", params, "Enviando", this, this);
        }

    }

    public void getSt(){
        cargado = true;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id_proyecto",proyect);
        WebBridge.send("listadostoryboard=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")){
                if (enviando) {
                    new AlertDialog.Builder(this)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle("Aviso")
                            .setMessage(json.getString("message"))
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    enviando=false;
                                    getSt();
                                }

                            })
                            .show();
                }else if(cargado){
                    JSONArray data1 = json.getJSONArray("data");
                    if (caj!=0){
                        caj--;
                        JSONObject act = data1.getJSONObject(caj);
                        loadStDet(act);
                    }
                    JSONObject placebo = new JSONObject();
                    data1.put(placebo);
                    RecyclerView.Adapter rvAdapter = new StoryListAdapter(data1,this);
                    recyclerList.setAdapter(rvAdapter);
                    recyclerList.getLayoutManager().scrollToPosition(data1.length()-2);
                    //recyclerList.scrollToPosition(recyclerList.getChildCount()-1);
                }else{
                    data2 = json.getJSONArray("data");
                    /*spinnerArray.add("Nuevo Proyecto");
                    id_proyectos.add(0);*/
                    for (int i = 0; i < data2.length(); i++) {
                        spinnerArray.add(data2.getJSONObject(i).getString("nombre"));
                        id_proyectos.add(data2.getJSONObject(i).getInt("id"));
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                            this, R.layout.spinner_r, spinnerArray);
                    adapter.setDropDownViewResource(R.layout.spinner_drop_r);
                    cy.setAdapter(adapter);
                    Log.e("texto-->", "se puso el texto");
                    Boolean first = (proyect!=0);
                    int gos = proyect;
                    int go=0;
                    cy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {

                            Log.i("renderSpinner -> ", "onItemSelected: " + myPosition + "/" + myID);
                            proyect = id_proyectos.get(myPosition);
                            getSt();
                            /*if (myPosition != 0) {
                                try {
                                    title.setText(data.getJSONObject(myPosition - 1).getString("nombre"));
                                    body.setText(data.getJSONObject(myPosition - 1).getString("descripcion"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                title.setText("");
                                body.setText("");
                            }*/
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });
                    if (first) {
                        for (int i = 0; i<id_proyectos.size();i++){
                            if (id_proyectos.get(i)==gos){
                                go=i;
                            }
                        }
                        cy.setSelection(go);
                        getSt();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                if (User.logged(this)) {User.clear(this);}
                //LoginManager.getInstance().logOut();
                Intent intent = new Intent(this, LogIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
