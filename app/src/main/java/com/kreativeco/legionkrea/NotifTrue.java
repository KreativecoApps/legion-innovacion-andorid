package com.kreativeco.legionkrea;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class NotifTrue extends AppCompatActivity implements WebBridge.WebBridgeListener{

    private FrameLayout noto;
    private TextView title, body, number;
    private Boolean isNot;
    private int not;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_not);
        noto = (FrameLayout) findViewById(R.id.lay_not);
        noto.setVisibility(View.VISIBLE);
        title = (TextView) findViewById(R.id.txt_title);
        body = (TextView) findViewById(R.id.txt_body);
        number = (TextView) findViewById(R.id.txt_number);
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("Nots_Activity", bundle);
        isNot= false;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        WebBridge.send("notificacionesuser=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Notif",this.getClass().getSimpleName());
    }

    public void loadNotifs(View view){
        Intent loginIntent = new Intent(this, Notificaciones.class);
        startActivity(loginIntent);
    }

    public void dissmissNot(View view){
        finish();
    }

    public void viewNot(View view){
        isNot=true;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id_notificacion",not);
        WebBridge.send("notificacionvista=1&key=admin", params, "Enviando", this, this);
    }

    public void loadNot(){
        isNot = false;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        WebBridge.send("notificacionesuser=1&key=admin", params, "Enviando", this, this);
    }

    public void closeSession(){
        if (User.logged(this)) {User.clear(this);}
        //LoginManager.getInstance().logOut();
        Intent intent = new Intent(this, LogIn.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {
        try {
            if (json.getBoolean("success")){
                if (isNot){
                    new AlertDialog.Builder(this)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setTitle("Aviso")
                            .setMessage("Notificación Vista")
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    loadNot();
                                }

                            })
                            .show();
                }else {
                    JSONArray nots = json.getJSONArray("data");
                    if (nots.length() > 0) {
                        number.setText(nots.length() + "");
                        body.setText(nots.getJSONObject(0).getString("notificacion"));
                        not = nots.getJSONObject(0).getInt("id_notificacion_user");
                    }else{
                        new AlertDialog.Builder(this)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle("Aviso")
                                .setMessage("No hay notificaciones")
                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        finish();
                                    }

                                })
                                .show();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                closeSession();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
