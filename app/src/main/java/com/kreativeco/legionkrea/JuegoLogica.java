package com.kreativeco.legionkrea;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.kreativeco.legionkrea.utils.User;
import com.kreativeco.legionkrea.utils.WebBridge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class JuegoLogica extends Activity implements WebBridge.WebBridgeListener{

    private DrawerLayout mDrawer;
    private RelativeLayout mDrawerOptions;
    CountDownTimer cTimer = null;
    private JSONArray data;
    private Boolean control = true;
    private TextView txtTime;
    private ImageView[] imagenes = new ImageView[10];
    private int[] cajas = {R.id.imageView, R.id.imageView2, R.id.imageView3, R.id.imageView4, R.id.imageView5, R.id.imageView6, R.id.imageView7, R.id.imageView8, R.id.imageView9, R.id.imageView0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.juego_l);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerOptions = (RelativeLayout) findViewById(R.id.left_drawer);
        ImageView btnOpenDrawer = (ImageView) findViewById(R.id.btn_open_drawer);
        btnOpenDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDrawer(v);
            }
        });
        for(int i=0;i<10;i++) {
            imagenes[i] = (ImageView) findViewById(cajas[i]);
            imagenes[i].setVisibility(View.INVISIBLE);
        }
        txtTime = (TextView) findViewById(R.id.txt_time);
        //loadGame();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Bundle bundle = new Bundle();
        bundle.putInt("user_id", Integer.parseInt(User.get("id_user",this)));
        mFirebaseAnalytics.setUserProperty("user_email", User.get("email",this));
        mFirebaseAnalytics.logEvent("Logica_Activity", bundle);
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        WebBridge.send("listadosolucion=1&key=admin", params, "Enviando", this, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        FirebaseAnalytics mFirebaseAnalytics;
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setCurrentScreen(this,"Juego de Logica",this.getClass().getSimpleName());
    }

    protected void onDestroy() {
        super.onDestroy();
        cancelTimer();
    }

    public void openDrawer(View view) {
        mDrawer.openDrawer(mDrawerOptions);
    }
    public void closeDrawer(View view) { mDrawer.closeDrawers(); }

    public void closeSesion (View view){
        Log.e("salio","primero");
        closeDrawer(mDrawer);
        if (User.logged(this)) {
            User.clear(this);}
        //LoginManager.getInstance().logOut();
        Intent intent = new Intent(this, LogIn.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity (intent);
        finish();

    }

    public void loadNProyect(View view){
        Intent loginIntent = new Intent(this, NewProyect.class);
        startActivity(loginIntent);
    }

    public void loadProfile(View view){
        Intent loginIntent = new Intent(this, Profile.class);
        startActivity(loginIntent);
    }

    public void loadProyects(View view){
        Intent loginIntent = new Intent(this, Proyects.class);
        startActivity(loginIntent);
    }

    public void loadRanking(View view){
        Intent loginIntent = new Intent(this, Ranking.class);
        startActivity(loginIntent);
    }

    public void loadLibrary(View view){
        Intent loginIntent = new Intent(this, Biblioteca.class);
        startActivity(loginIntent);
    }

    public void loadNots(View view){
        Intent loginIntent = new Intent(this, NotifTrue.class);
        startActivity(loginIntent);
    }

    public void loadAjustes(View view){
        Intent loginIntent = new Intent(this, Ajustes.class);
        startActivity(loginIntent);
    }

    public void goHome(View view){
        Intent loginIntent = new Intent(this, Home.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
    }

    void loadGame() {
        Log.e("entro->","timer");
        cTimer = new CountDownTimer(6000, 1000) {

            public void onTick(long millisUntilFinished) {

                long remainingTime = millisUntilFinished / 1000;


                if (remainingTime > 9) {
                    txtTime.setText("0:" + millisUntilFinished / 1000+"");

                } else {
                    txtTime.setText("0:0" + millisUntilFinished / 1000+"");

                }
            }

            public void onFinish() {
                txtTime.setText("0:00");
                sendResponse(0);
            }
        };
        cTimer.start();
    }

    //cancel timer
    void cancelTimer() {
        if (cTimer != null)
            cTimer.cancel();
    }


    public void sendResponse(int response){
        cancelTimer();
        control = false;
        HashMap<String, Object> params = new HashMap<>();
        params.put("token", User.getToken(this));
        params.put("id_respuesta", response);
        WebBridge.send("guardarsolucion=1&key=admin", params, "Enviando", this, this);
    }

    public  void close(View view){
        finish();
    }

    public  void clickBack(View view){
        finish();
    }

    public void next(View view){
        finish();
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONObject json) {

        try {
            if (json.getBoolean("success")){
                if (control) {
                    data = json.getJSONArray("data");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject act = data.getJSONObject(i);
                        imagenes[i].setVisibility(View.VISIBLE);
                        String imag = act.getString("imagen");
                        //Log.e("imagen:", imag);
                        Glide.with(JuegoLogica.this).load(imag).into(imagenes[i]);
                        Log.e("imagen:", imag);
                        //idsO.add(act.getInt("id"));
                        final int finalI = i;
                        imagenes[i].setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                imagenes[finalI].setVisibility(View.INVISIBLE);
                                try {
                                    sendResponse(data.getJSONObject(finalI).getInt("id"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                    loadGame();
                }else{
                    cancelTimer();
                    new android.app.AlertDialog.Builder(JuegoLogica.this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle("Aviso")
                        .setMessage(json.getString("msg"))
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        })
                        .show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWebBridgeSuccess(String url, JSONArray json) {

    }

    @Override
    public void onWebBridgeFailure(String url, String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("error_message").equals("token_not_provided")){
                if (User.logged(this)) {User.clear(this);}
                //LoginManager.getInstance().logOut();
                Intent intent = new Intent(this, LogIn.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }else{
                new android.app.AlertDialog.Builder(JuegoLogica.this)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle("Error")
                    .setMessage(json.getString("error_message"))
                    .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    })
                    .show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        cancelTimer();
        finish();
    }
}
